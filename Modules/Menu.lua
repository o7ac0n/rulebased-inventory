-----------------------------------------------------------------------------------
-- Addon Name: Rulebased Inventory
-- Creator: TaxTalis
-- Addon Ideal: user-defined rule-based actions for inventory application
-- Addon Creation Date: September 11, 2018
--
-- File Name: Menu.lua
-- File Description: This file contains the addon menu
-- Load Order Requirements: after Loader
--
-----------------------------------------------------------------------------------

local RbI = RulebasedInventory
RbI.menu = RbI.menu or {}
local ruleEnv = RbI.ruleEnvironment

local rbiMenu = {}

local dividerFullOpaque = {
	type = 'divider',
	height = 10,
	alpha = 0.5,
	width = 'full',
}
local dividerHalfInvisible = {
	type = 'divider',
	height = 10,
	alpha = 0,
	width = 'half',
}

local function getSelectedTaskId()
	return RbI.menu.selectedTask
end

local function getSelectedTask(taskId)
	return RbI.tasks.internal[taskId or getSelectedTaskId()]
end

function RbI.menu.Initialize()
	local LAM2 = LibAddonMenu2

	local waitForPanel = LAM2:RegisterAddonPanel('RBISettingsPanel', {
		type = 'panel',
		name = RbI.title,
		displayName = RbI.title,
		author = RbI.author,
		version = tostring(RbI.addonVersion),
		website = RbI.website,
		slashCommand = '/rbi',
		registerForRefresh = true,
		registerForDefaults = false
	})
	LAM2:RegisterOptionControls('RBISettingsPanel', rbiMenu.createMenues())

	local function ResizeRuleContentEditBox()
		-- minimize to standard
		RbIRuleContent.container:SetHeight(24 * 3)
		-- extend the height of container and parent to the number of not visible lines
		RbIRuleContent.container:SetHeight(RbIRuleContent.container:GetHeight() + (24 * RbIRuleContent.editbox:GetScrollExtents()))
		RbIRuleContent:SetHeight(RbIRuleContent.container:GetHeight())
	end

	-- register function to customize the menu on creation
	CALLBACK_MANAGER:RegisterCallback("LAM-PanelControlsCreated",
		function(panel)
			if (panel == waitForPanel) then
				waitForPanel = nil

				ResizeRuleContentEditBox()
				-- set handler to change size of the rulecontent-editbox on the go (thanks, sirinsidiator!)
				RbIRuleContent.editbox:SetHandler("OnTextChanged", ResizeRuleContentEditBox, "RuleBasedInventory")

				RbI.menu.UpdateTaskChoices()
			end
		end)
	RbI.menu.Initialize = nil
end

function rbiMenu.createMenues()
	rbiMenu.createFunctions()

	local menu = {}
	local control = {}
	local submenu = {}
	local subsubmenu = {}

	-- initialize menu-data
	-- profile
	RbI.menu.selectedProfile = RbI.Profile.GetCurrentProfileName()
	RbI.menu.newProfile = ''
	
	-- rule
	RbI.menu.selectedRule = RbI.menu.GetLexicographicallyFirst(RbI.rules.list)
	if(RbI.menu.selectedRule ~= nil) then
		RbI.menu.selectedRuleContent = RbI.rules.user[RbI.menu.selectedRule].GetContent()
	else
		RbI.menu.selectedRuleContent = ''
	end
	RbI.menu.newRule = ''
	
	-- task
	RbI.menu.selectedTask = RbI.menu.GetLexicographicallyFirst(RbI.tasks.list)
	
	-- ruleSet
	RbI.menu.ruleSetList = RbI.RuleSet.GetList()
	RbI.menu.selectedRuleSet = RbI.RuleSet.GetCurrentRuleSetName(RbI.menu.selectedTask)
	RbI.menu.newRuleSet = ''
	RbI.menu.currentAllRules = RbI.RuleSet.GetRules(RbI.menu.selectedTask, RbI.FIT_ALL)
	RbI.menu.currentAnyRules = RbI.RuleSet.GetRules(RbI.menu.selectedTask, RbI.FIT_ANY)
	RbI.menu.availableAllRules = RbI.RuleSet.GetAvailableRules(RbI.menu.selectedTask, RbI.FIT_ALL)
	RbI.menu.availableAnyRules = RbI.RuleSet.GetAvailableRules(RbI.menu.selectedTask, RbI.FIT_ANY)


	table.insert(menu, rbiMenu.createProfiles())
	table.insert(menu, rbiMenu.createTasks())
	table.insert(menu, rbiMenu.createRules())
	table.insert(menu, rbiMenu.createSettings())

	if (RbI.class.DataDumper ~= nil) then -- only for test-environment
		for key, fn in pairs(RbI.class.DataDumper) do
			table.insert(menu, {
				type = 'button',
				name = key,
				func = fn,
				width = 'half',
			})
		end
		table.insert(menu, {
			type = 'description',
			text = function() return "File load time: " .. (RbI.fileLoadTime / 1000) .. " secs" end,
			width = 'full',
		})
	end

	return menu
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Extern functions ----------------------------------------------------------------------
------------------------------------------------------------------------------
function rbiMenu.createFunctions()
	function RbI.menu.UpdateRuleChoices(ruleName)
		RbIRuleDropdown:UpdateChoices(RbI.rules.list)
		local ruleName = ruleName or RbI.menu.GetLexicographicallyFirst(RbI.rules.list)
		RbI.menu.SetSelectedRule(ruleName)
		RbI.menu.UpdateRuleSetRules()
	end

	function RbI.menu.GetLexicographicallyFirst(tbl)
		local result = nil
		for i, v in pairs(tbl) do
			if (result == nil or result > v) then
				result = v
			end
		end
		return result
	end

	-- functions to update dropdowns

	-- profile
	function RbI.menu.UpdateProfileChoices()
		RbIProfileDropdown:UpdateChoices(RbI.Profile.GetList())
	end

	function RbI.menu.SetSelectedProfile()
		RbI.menu.selectedProfile = RbI.Profile.GetCurrentProfileName()
	end

	-- ruleSet
	function RbI.menu.UpdateRuleSetRules(currentAllRule, availableAllRule, currentAnyRule, availableAnyRule)
		RbIRuleSetAllCurrentRuleDropdown:UpdateChoices(RbI.RuleSet.GetRules(RbI.menu.selectedTask, RbI.FIT_ALL))
		RbIRuleSetAllAvailableRuleDropdown:UpdateChoices(RbI.RuleSet.GetAvailableRules(RbI.menu.selectedTask, RbI.FIT_ALL))
		RbIRuleSetAnyCurrentRuleDropdown:UpdateChoices(RbI.RuleSet.GetRules(RbI.menu.selectedTask, RbI.FIT_ANY))
		RbIRuleSetAnyAvailableRuleDropdown:UpdateChoices(RbI.RuleSet.GetAvailableRules(RbI.menu.selectedTask, RbI.FIT_ANY))
		RbI.menu.ruleSetCurrentAllRule = currentAllRule
		RbI.menu.ruleSetAvailableAllRule = availableAllRule
		RbI.menu.ruleSetCurrentAnyRule = currentAnyRule
		RbI.menu.ruleSetAvailableAnyRule = availableAnyRule
	end

	function RbI.menu.UpdateRuleSetChoices(currentAllRule, availableAllRule, currentAnyRule, availableAnyRule)
		if (RbIRuleSetDropdown ~= nil) then
			RbIRuleSetDropdown:UpdateChoices(RbI.RuleSet.GetList())
		end
		RbI.menu.SetSelectedProfile()
		RbI.menu.selectedRuleSet = RbI.RuleSet.GetCurrentRuleSetName(RbI.menu.selectedTask)
		RbI.menu.UpdateRuleSetRules(currentAllRule, availableAllRule, currentAnyRule, availableAnyRule)
	end

	-- Task
	function RbI.menu.SetSelectedTask(taskName)
		RbI.menu.selectedTask = taskName
		RbI.menu.UpdateRuleSetChoices()
	end

	function RbI.menu.UpdateTaskChoices()
		local tooltips = {}
		local displayNames = {}
		for _, taskId in pairs(RbI.tasks.list) do
			local task = RbI.tasks.internal[taskId]
			tooltips[#tooltips + 1] = task.GetDescription()
			displayNames[#displayNames + 1] = task.GetDisplayName()
		end
		RbITaskDropdown:UpdateChoices(displayNames, RbI.tasks.list, tooltips)
		RbITaskDropdown.dropdown:SetSelectedItem(RbI.menu.selectedTask)
	end

	-- Rule
	function RbI.menu.SetSelectedRule(ruleName)
		RbI.menu.selectedRule = ruleName
		if (ruleName ~= nil) then
			RbI.menu.selectedRuleContent = RbI.rules.user[ruleName].GetContent()
		else
			RbI.menu.selectedRuleContent = ''
		end
	end
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- PROFILE -------------------------------------------------------------------
------------------------------------------------------------------------------
function rbiMenu.createProfiles()
	local profilesMenu = {
		type = 'submenu',
		name = 'Profiles',
		controls = {},
	}

	-- Dropdown
	table.insert(profilesMenu.controls, {
		type = 'dropdown',
		choices = RbI.Profile.GetList(),
		getFunc = function() return RbI.menu.selectedProfile end,
		setFunc = function(profileName) RbI.menu.selectedProfile = profileName end,
		width = 'half',
		reference = 'RbIProfileDropdown',
		disabled = function() return #RbI.Profile.GetList() == 0 end,
		scrollable = true,
		sort = 'name-up',
	})

	-- Load
	table.insert(profilesMenu.controls, {
		type = 'button',
		name = 'Load',
		func = function() RbI.Profile.Load(RbI.menu.selectedProfile)
			RbI.menu.UpdateRuleSetChoices()
		end,
		width = 'half',
		disabled = function() return RbI.menu.selectedProfile == RbI.Profile.GetCurrentProfileName() end,
	})

	-- Delete
	table.insert(profilesMenu.controls, {
		type = 'button',
		name = 'Delete',
		func = function() RbI.Profile.Delete(RbI.menu.selectedProfile)
			RbI.menu.UpdateProfileChoices()
			RbI.menu.selectedProfile = nil
		end,
		width = 'half',
		disabled = function() return RbI.menu.selectedProfile == nil end,
		isDangerous = true,
		warning = 'Deleting a profile cannot be undone. Continue?',
	})

	-- Save
	table.insert(profilesMenu.controls, {
		type = 'button',
		name = 'Save',
		func = function() RbI.Profile.Save(RbI.menu.selectedProfile) end,
		width = 'half',
		disabled = function() return RbI.Profile.IsLikeCurrent(RbI.menu.selectedProfile) end,
		isDangerous = true,
		warning = 'Selected profile will be overwritten with current settings. Continue?',
	})

	-- New
	table.insert(profilesMenu.controls, dividerFullOpaque)

	table.insert(profilesMenu.controls, {
		type = 'editbox',
		getFunc = function() return '' end,
		setFunc = function(profileName) RbI.menu.newProfile = profileName end,
		isMultiline = false,
		isExtraWide = false,
		maxChars = 100,
		width = 'half',
	})

	table.insert(profilesMenu.controls, {
		type = 'button',
		name = 'New',
		func = function() RbI.Profile.Save(RbI.menu.newProfile)
			RbI.menu.UpdateProfileChoices()
			RbI.menu.selectedProfile = RbI.menu.newProfile
			RbI.menu.newProfile = ''
		end,
		width = 'half',
		disabled = function() return RbI.menu.newProfile == ''
				or RbI.Profile.Exists(RbI.menu.newProfile)
		end,
	})

	return profilesMenu
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- TASK ----------------------------------------------------------------------
------------------------------------------------------------------------------
function rbiMenu.createTasks()
	local tasksMenu = {
		type = 'submenu',
		name = 'Tasks',
		controls = {},
	}

	-- Dropdown
	table.insert(tasksMenu.controls, {
		type = 'dropdown',
		choices = RbI.tasks.list,
		getFunc = function() return RbI.menu.selectedTask end,
		setFunc = function(taskName) RbI.menu.SetSelectedTask(taskName) end,
		width = 'half',
		scrollable = 13,
		-- tooltip = function() return RbI.tasks.internal[RbI.menu.selectedTask].GetDescription() end, -- currently doesn't work with LibAddonMenu
		reference = 'RbITaskDropdown',
	})

	-- Run Task
	table.insert(tasksMenu.controls, {
		type = 'button',
		name = 'Run',
		func = function()
			local taskId = getSelectedTaskId()
			local Task1 = getSelectedTask(taskId)
			local Task2 = Task1.GetOpposingTask()
			RbI.TaskStarter(Task1, nil, nil, nil, Task2)
		end,
		width = 'half',
		disabled = function() return not RbI.RuleSet.HasRules(getSelectedTaskId()) or RbI.ActionQueue.IsRunning() or RbI.testrun or not getSelectedTask().GetDefaultAllowAction() end,
		reference = 'RbITaskRunButton',
	})

	-- Delete All Rules
	table.insert(tasksMenu.controls, {
		type = 'button',
		name = 'Remove Rule(s)',
		func = function() RbI.RuleSet.RemoveAllRules(RbI.menu.selectedTask)
			RbI.menu.UpdateRuleSetChoices()
		end,
		width = 'half',
		tooltip = 'Removes all assigned rules from this task. So this will deactivating the task.',
		disabled = function() return RbI.RuleSet.HasRules(RbI.menu.selectedTask) == false end,
	})

	-- Test Task
	table.insert(tasksMenu.controls, {
		type = 'button',
		name = 'Test',
		func = function()
			RbI.testrun = true
			local task = RbI.tasks.internal[RbI.menu.selectedTask]
			RbI.TaskStarter(task)
			if (task.RunWithRevertTask()) then
				RbI.TaskStarter(task.GetOpposingTask())
			end
		end,
		width = 'half',
		disabled = function() return RbI.ActionQueue.IsRunning() or RbI.testrun or (RbI.RuleSet.HasRules(RbI.menu.selectedTask) == false) end,
		tooltip = 'Checks which items would be used by this task. The task works on its standard bag.',
		reference = 'RbITaskTestButton',
	})

	-- Untask setting
	table.insert(tasksMenu.controls, {
		type = 'checkbox',
		name = '   With opposite check',
		tooltip = 'Also executes the opposite task to unjunk/unmark items.',
		getFunc = function() return getSelectedTask().RunWithRevertTask() end,
		setFunc = function(value)
			local taskId = getSelectedTaskId()
			local taskSettings = RbI.account.tasks[taskId]
			if (value) then
				if (not taskSettings) then
					taskSettings = {}
					RbI.account.tasks[taskId] = taskSettings
				end
				taskSettings.runWithRevertTask = true
			else
				taskSettings.runWithRevertTask = nil
			end
		end,
		disabled = function()
			local task = getSelectedTask()
			return not task.HasRevertTask()
		end,
		width = 'half',
	})

	-- RULES-SET -----------------------------------------------------------------
	------------------------------------------------------------------------------

	if (RbI.account.settings.usingRuleSets) then
		local subtasksMenu = {
			name = 'RuleSet',
			type = 'submenu',
			controls = {},
		}

		-- Dropdown
		table.insert(subtasksMenu.controls, {
			type = 'dropdown',
			choices = RbI.RuleSet.GetList(),
			getFunc = function() return RbI.menu.selectedRuleSet end,
			setFunc = function(ruleSetName) RbI.menu.selectedRuleSet = ruleSetName end,
			width = 'half',
			reference = 'RbIRuleSetDropdown',
			sort = 'name-up',
			scrollable = true,
			disabled = function() return #RbI.RuleSet.GetList() == 0 end,
		})

		-- Load
		local control = {}
		control.type = 'button'
		control.name = 'Load'
		control.func = function() RbI.RuleSet.Load(RbI.menu.selectedTask, RbI.menu.selectedRuleSet)
			RbI.menu.UpdateRuleSetChoices()
		end
		control.width = 'half'
		control.disabled = function() return RbI.menu.selectedRuleSet == nil
				or RbI.menu.selectedRuleSet == RbI.RuleSet.GetCurrentRuleSetName(RbI.menu.selectedTask)
		end
		table.insert(subtasksMenu.controls, control)

		-- Delete
		control = {}
		control.type = 'button'
		control.name = 'Delete'
		control.func = function() RbI.RuleSet.Delete(RbI.menu.selectedRuleSet)
			RbI.menu.UpdateRuleSetChoices()
		end
		control.width = 'half'
		control.disabled = function() return RbI.menu.selectedRuleSet == nil end
		control.isDangerous = true
		control.warning = 'Deleting a ruleSet cannot be undone!'
		table.insert(subtasksMenu.controls, control)

		-- Save
		control = {}
		control.type = 'button'
		control.name = 'Save'
		control.func = function() RbI.RuleSet.Save(RbI.menu.selectedTask, RbI.menu.selectedRuleSet)
			RbI.menu.UpdateRuleSetChoices()
		end
		control.width = 'half'
		control.disabled = function() return RbI.RuleSet.IsLikeCurrent(RbI.menu.selectedTask, RbI.menu.selectedRuleSet) end
		control.isDangerous = true
		control.warning = 'Selected ruleSet will be overwritten with current rules. Continue?'
		table.insert(subtasksMenu.controls, control)

		-- New
		table.insert(subtasksMenu.controls, dividerFullOpaque)
		control = {}
		control.type = 'editbox'
		control.getFunc = function() return '' end
		control.setFunc = function(ruleSetName) RbI.menu.newRuleSet = ruleSetName end
		control.isMultiline = false
		control.isExtraWide = false
		control.maxChars = 100
		control.width = 'half'
		table.insert(subtasksMenu.controls, control)

		control = {}
		control.type = 'button'
		control.name = 'New'
		control.func = function() RbI.RuleSet.Save(RbI.menu.selectedTask, RbI.menu.newRuleSet)
			RbI.menu.UpdateRuleSetChoices()
			RbI.menu.selectedRuleSet = RbI.menu.newRuleSet
			RbI.menu.newRuleSet = ''
		end
		control.width = 'half'
		control.disabled = function() return RbI.menu.newRuleSet == ''
				or RbI.RuleSet.Exists(RbI.menu.newRuleSet)
		end
		table.insert(subtasksMenu.controls, control)
		table.insert(tasksMenu.controls, subtasksMenu)
	end


	-- Rule Selection
	local subtasksMenu = {
		name = 'Item has to fit all of these rules',
		type = 'submenu',
		controls = {},
	}

	-- current rules dropdown
	local control = {}
	control.name = 'Current Rules'
	control.type = 'dropdown'
	control.choices = RbI.RuleSet.GetRules(RbI.menu.selectedTask, 1)
	control.getFunc = function() return RbI.menu.ruleSetCurrentAllRule end
	control.setFunc = function(ruleName) RbI.menu.ruleSetCurrentAllRule = ruleName end
	control.width = 'half'
	control.disabled = function() return #RbI.RuleSet.GetRules(RbI.menu.selectedTask, RbI.FIT_ALL) == 0 end
	control.sort = 'name-up'
	control.scrollable = true
	control.reference = 'RbIRuleSetAllCurrentRuleDropdown'
	table.insert(subtasksMenu.controls, control)

	-- delete rule from ruleSet
	control = {}
	control.type = 'button'
	control.name = 'Delete'
	control.func = function() RbI.RuleSet.RemoveRule(RbI.menu.selectedTask, RbI.menu.ruleSetCurrentAllRule)
		RbI.menu.UpdateRuleSetChoices(nil, RbI.menu.ruleSetCurrentAllRule)
	end
	control.width = 'half'
	control.disabled = function() return RbI.menu.ruleSetCurrentAllRule == nil end
	table.insert(subtasksMenu.controls, control)

	-- add rules dropdown
	control = {}
	control.type = 'dropdown'
	control.name = 'Available Rules'
	control.choices = RbI.RuleSet.GetAvailableRules(RbI.menu.selectedTask, RbI.FIT_ALL)
	control.getFunc = function() return RbI.menu.ruleSetAvailableAllRule end
	control.setFunc = function(ruleName) RbI.menu.ruleSetAvailableAllRule = ruleName end
	control.width = 'half'
	control.disabled = function() return #RbI.RuleSet.GetAvailableRules(RbI.menu.selectedTask, RbI.FIT_ALL) == 0 end
	control.sort = 'name-up'
	control.scrollable = true
	control.reference = 'RbIRuleSetAllAvailableRuleDropdown'
	table.insert(subtasksMenu.controls, control)

	-- add rule to ruleSet
	control = {}
	control.type = 'button'
	control.name = 'Add'
	control.func = function() RbI.RuleSet.AddRule(RbI.menu.selectedTask, RbI.menu.ruleSetAvailableAllRule, RbI.FIT_ALL)
		RbI.menu.UpdateRuleSetChoices(RbI.menu.ruleSetAvailableAllRule)
	end
	control.width = 'half'
	control.disabled = function() return RbI.menu.ruleSetAvailableAllRule == nil end
	table.insert(subtasksMenu.controls, control)
	table.insert(tasksMenu.controls, subtasksMenu)


	local subtasksMenu = {
		name = 'Item has to fit any of these rules',
		type = 'submenu',
		controls = {},
	}

	-- current rules dropdown
	control = {}
	control.name = 'Current Rules'
	control.type = 'dropdown'
	control.choices = RbI.RuleSet.GetRules(RbI.menu.selectedTask, RbI.FIT_ANY)
	control.getFunc = function() return RbI.menu.ruleSetCurrentAnyRule end
	control.setFunc = function(ruleName) RbI.menu.ruleSetCurrentAnyRule = ruleName end
	control.width = 'half'
	control.disabled = function() return #RbI.RuleSet.GetRules(RbI.menu.selectedTask, RbI.FIT_ANY) == 0 end
	control.sort = 'name-up'
	control.scrollable = true
	control.reference = 'RbIRuleSetAnyCurrentRuleDropdown'
	table.insert(subtasksMenu.controls, control)

	-- delete rule from ruleSet
	control = {}
	control.type = 'button'
	control.name = 'Delete'
	control.func = function() RbI.RuleSet.RemoveRule(RbI.menu.selectedTask, RbI.menu.ruleSetCurrentAnyRule)
		RbI.menu.UpdateRuleSetChoices(nil, nil, nil, RbI.menu.ruleSetCurrentAnyRule)
	end
	control.width = 'half'
	control.disabled = function() return RbI.menu.ruleSetCurrentAnyRule == nil end
	table.insert(subtasksMenu.controls, control)

	-- add rules dropdown
	control = {}
	control.type = 'dropdown'
	control.name = 'Available Rules'
	control.choices = RbI.RuleSet.GetAvailableRules(RbI.menu.selectedTask, RbI.FIT_ANY)
	control.getFunc = function() return RbI.menu.ruleSetAvailableAnyRule end
	control.setFunc = function(ruleName) RbI.menu.ruleSetAvailableAnyRule = ruleName end
	control.width = 'half'
	control.disabled = function() return #RbI.RuleSet.GetAvailableRules(RbI.menu.selectedTask, RbI.FIT_ANY) == 0 end
	control.sort = 'name-up'
	control.scrollable = true
	control.reference = 'RbIRuleSetAnyAvailableRuleDropdown'
	table.insert(subtasksMenu.controls, control)

	-- add rule to ruleSet
	control = {}
	control.type = 'button'
	control.name = 'Add'
	control.func = function() RbI.RuleSet.AddRule(RbI.menu.selectedTask, RbI.menu.ruleSetAvailableAnyRule, RbI.FIT_ANY)
		RbI.menu.UpdateRuleSetChoices(nil, nil, RbI.menu.ruleSetAvailableAnyRule)
	end
	control.width = 'half'
	control.disabled = function() return RbI.menu.ruleSetAvailableAnyRule == nil end
	table.insert(subtasksMenu.controls, control)
	table.insert(tasksMenu.controls, subtasksMenu)

	return tasksMenu
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- RULES ---------------------------------------------------------------------
------------------------------------------------------------------------------
function rbiMenu.createRules()
	local rulesMenu = {
		type = 'submenu',
		name = 'Rules',
		controls = {},
	}

	-- Dropdown
	table.insert(rulesMenu.controls, {
		type = 'dropdown',
		choices = RbI.rules.list,
		getFunc = function() return RbI.menu.selectedRule end,
		setFunc = function(ruleName) RbI.menu.SetSelectedRule(ruleName) end,
		width = 'half',
		reference = 'RbIRuleDropdown',
		scrollable = true,
		sort = 'name-up',
	})

	-- Delete
	table.insert(rulesMenu.controls, {
		type = 'button',
		name = 'Delete',
		func = function() RbI.rules.user[RbI.menu.selectedRule].Delete() end,
		width = 'half',
		disabled = function() return RbI.menu.selectedRule == nil end,
		isDangerous = true,
		warning = 'Deleting a rule cannot be undone!',
	})

	-- Content
	table.insert(rulesMenu.controls, {
		type = 'editbox',
		--name = 'Content',
		getFunc = function() return RbI.menu.selectedRuleContent end,
		setFunc = function(content) RbI.menu.selectedRuleContent = content end,
		isMultiline = true,
		isExtraWide = true,
		maxChars = 2000, -- saveVariables can not save strings larger than 2000
		disabled = function() return RbI.menu.selectedRule == nil end,
		width = 'full',
		reference = 'RbIRuleContent',
	})

	-- Cancel
	table.insert(rulesMenu.controls, {
		type = 'button',
		name = 'Cancel',
		func = function() RbI.menu.selectedRuleContent = RbI.rules.user[RbI.menu.selectedRule].GetContent() end,
		width = 'half',
		disabled = function() return RbI.menu.selectedRule == nil or RbI.menu.selectedRuleContent == RbI.rules.user[RbI.menu.selectedRule].GetContent() end,
	})

	-- Test Rule
	table.insert(rulesMenu.controls, {
		type = 'button',
		name = 'Test',
		func = function() ruleEnv.Test(RbI.menu.selectedRule, RbI.menu.selectedRuleContent) end,
		width = 'half',
		disabled = function() return RbI.menu.selectedRule == nil end,
		tooltip = 'Checks which items you have selected with this rule. Items from backpack, bank and craftbag will be checked.',
		reference = 'RbIRuleTestButton',
	})

	-- empty space
	table.insert(rulesMenu.controls, dividerHalfInvisible)

	-- Save
	table.insert(rulesMenu.controls, {
		type = 'button',
		name = 'Save',
		func = function() RbI.rules.user[RbI.menu.selectedRule].SetContent(RbI.menu.selectedRuleContent) end,
		width = 'half',
		disabled = function() return RbI.menu.selectedRule == nil or RbI.menu.selectedRuleContent == RbI.rules.user[RbI.menu.selectedRule].GetContent() end,
	})

	-- New
	table.insert(rulesMenu.controls, dividerFullOpaque)
	table.insert(rulesMenu.controls, {
		type = 'editbox',
		getFunc = function() return '' end,
		setFunc = function(ruleName) RbI.menu.newRule = ruleName end,
		isMultiline = false,
		isExtraWide = false,
		maxChars = 100,
		width = 'half',
	})

	table.insert(rulesMenu.controls, {
		type = 'button',
		name = 'New',
		func = function() ruleEnv.CreateUserRule(RbI.menu.newRule)
			RbI.menu.newRule = ''
		end,
		width = 'half',
		disabled = function() return RbI.menu.newRule == ''
				or RbI.rules.user[RbI.menu.newRule] ~= nil
				or RbI.rules.user[string.lower(RbI.menu.newRule)] ~= nil
		end,
	})

	-- empty space
	table.insert(rulesMenu.controls, dividerHalfInvisible)

	table.insert(rulesMenu.controls, {
		type = 'button',
		name = 'Rename',
		func = function() local newRuleName = RbI.menu.newRule
			RbI.RenameRule(RbI.menu.selectedRule, newRuleName)
			RbI.menu.UpdateRuleChoices(newRuleName)
			RbI.menu.newRule = ''
		end,
		width = 'half',
		disabled = function() return RbI.menu.newRule == ''
				or RbI.rules.user[RbI.menu.newRule] ~= nil
				or RbI.rules.user[string.lower(RbI.menu.newRule)] ~= nil
		end,
	})

	return rulesMenu
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- SETTINGS ------------------------------------------------------------------
------------------------------------------------------------------------------
function rbiMenu.createSettings()
	local settingsMenu = {
		type = 'submenu',
		name = 'Settings',
		controls = {},
	}
	--------- Additionals/Features -------------------------------------------------------------
	-- table.insert(settingsMenu.controls, { type = "header", name = "Additionals", width = "full", })

	-- Item Inspector
	table.insert(settingsMenu.controls, {
		type = 'checkbox',
		name = 'Item Inspector Context Menu',
		tooltip = 'Adds an entry to the context menu (\'' .. RbI.CONTEXT_MENU_ENTRY .. '\') in inventory with which main item data used by RbI can be printed out. Needs LibCustomMenu addon!!!',
		getFunc = function() return (AddCustomMenuItem ~= nil) and RbI.account.settings.contextMenu end,
		setFunc = function(value) RbI.account.settings.contextMenu = value end,
		disabled = function() return (AddCustomMenuItem == nil) end,
		width = 'full',
		requiresReload = true,
	})

	-- Initial item-check
	table.insert(settingsMenu.controls, {
		type = 'checkbox',
		name = 'Initial item-check',
		tooltip = 'After login all backpack items will be check for following tasks: FCOISMark (+FCOISUnmark), Junk (+UnJunk), Destroy, Use',
		getFunc = function() return RbI.account.settings.startupitemcheck end,
		setFunc = function(value) RbI.account.settings.startupitemcheck = value end,
		width = 'full',
	})

	-- case-sensitivity
	table.insert(settingsMenu.controls, {
		type = 'checkbox',
		name = 'Rule case-sensitivity/Custom user-functions',
		tooltip = 'The default is \'false\', with lowercasing a rule completely before executing, prevent potential syntax errors. With this option this can be deactivated. So the case for function calls and operators won\'t be ignored for rule definition anymore. This has to be used to create custom user functions to call ESO-Api functions.',
		getFunc = function() return RbI.account.settings.casesensitiverules end,
		setFunc = function(value) RbI.account.settings.casesensitiverules = value end,
		width = 'full',
	})

	-- Using Rulesets
	table.insert(settingsMenu.controls, {
		type = 'checkbox',
		name = 'Using Rulesets',
		tooltip = 'Rulesets are used to predefine a combination of rules, with which tasks can be set.',
		getFunc = function() return RbI.account.settings.usingRuleSets end,
		setFunc = function(value) RbI.account.settings.usingRuleSets = value end,
		width = 'full',
		requiresReload = true,
	})

	--------- Safety -------------------------------------------------------------
	table.insert(settingsMenu.controls, { type = "header", name = "Safety", width = "full", })

	-- saferule
	table.insert(settingsMenu.controls, {
		type = 'checkbox',
		name = 'Use internal Safetyrule',
		tooltip = 'When activated the internal safetyrule is used for the tasks: Deconstruct, Extract, Fence, Sell, Destroy. \n\nSafetyrule currently defined as: \n' .. RbI.internalRules.Safety,
		getFunc = function() return RbI.account.settings.safetyrule end,
		setFunc = function(value) RbI.account.settings.safetyrule = value end,
		width = 'full',
	})

	-- fcois
	table.insert(settingsMenu.controls, {
		type = 'checkbox',
		name = 'Enforce FCOIS',
		tooltip = 'Enforces that FCOItemSaver is running. Regardless of this option: when FCOIS is installed its marks will be considered. When this setting is set to \'true\' and FCOIS isn\'t installed only BagToBank, BankToBag and CraftToBag can be executed',
		getFunc = function() return RbI.account.settings.fcois end,
		setFunc = function(value) RbI.account.settings.fcois = value end,
		width = 'full',
	})

	--------- Output -------------------------------------------------------------
	table.insert(settingsMenu.controls, { type = "header", name = "Output", width = "full", })

	-- taskStart
	table.insert(settingsMenu.controls, {
		type = 'checkbox',
		name = 'Task Start Message',
		tooltip = 'Only for batch tasks (BagToBank, BankToBag, Deconstruct, Extract, Refine, Fence, Launder, Sell)',
		getFunc = function() return RbI.account.settings.taskStartOutput end,
		setFunc = function(value) RbI.account.settings.taskStartOutput = value end,
		width = 'full',
	})

	-- taskOutput
	table.insert(settingsMenu.controls, {
		type = 'checkbox',
		name = 'Task Action Output',
		tooltip = 'For all tasks',
		getFunc = function() return RbI.account.settings.taskActionOutput end,
		setFunc = function(value) RbI.account.settings.taskActionOutput = value end,
		width = 'full',
	})

	-- taskSummary
	table.insert(settingsMenu.controls, {
		type = 'checkbox',
		name = 'Task Summary Output',
		tooltip = 'Currently only for tasks \'Sell\' and \'Fence\'',
		getFunc = function() return RbI.account.settings.taskSummaryOutput end,
		setFunc = function(value) RbI.account.settings.taskSummaryOutput = value end,
		width = 'full',
	})

	-- taskFinish
	table.insert(settingsMenu.controls, {
		type = 'checkbox',
		name = 'Task Finish Message',
		tooltip = 'Only for batch tasks (BagToBank, BankToBag, Deconstruct, Extract, Refine, Fence, Launder, Sell)',
		getFunc = function() return RbI.account.settings.taskFinishOutput end,
		setFunc = function(value) RbI.account.settings.taskFinishOutput = value end,
		width = 'full',
	})

	-- color
	table.insert(settingsMenu.controls, {
		type = 'colorpicker',
		name = 'Chat output color',
		tooltip = 'The output color for all output texts in the chat',
		getFunc = function() return ZO_ColorDef:New(RbI.account.settings.outputcolor):UnpackRGBA() end,
		setFunc = function(r, g, b, a) RbI.account.settings.outputcolor = ZO_ColorDef:New(r, g, b, a):ToHex() end,
		width = 'full',
	})

	--------- Timing -------------------------------------------------------------
	table.insert(settingsMenu.controls, { type = "header", name = "Timing", width = "full", })

	-- immediate execution at craftStation
	table.insert(settingsMenu.controls, {
		type = 'checkbox',
		name = 'CraftStations Immediate Execution',
		tooltip = 'Automatically executes all deconstruction, extraction and refinement when interacting with a corresponding craftingstation, without having to open the respective tabs first. (Tasks: Deconstruct, Extract, Refine)',
		getFunc = function() return RbI.account.settings.immediateExecutionAtCraftStation end,
		setFunc = function(value) RbI.account.settings.immediateExecutionAtCraftStation = value end,
		width = 'full',
	})

	-- action delay slider
	table.insert(settingsMenu.controls, {
		type = 'slider',
		name = 'Delay between actions',
		getFunc = function() return RbI.account.settings.actionDelay end,
		setFunc = function(value) RbI.account.settings.actionDelay = value end,
		width = 'full',
		min = 1,
		max = 200,
		step = 1,
		clampInput = true,
	})

	-- task delay slider
	table.insert(settingsMenu.controls, {
		type = 'slider',
		name = 'Delay before tasks',
		getFunc = function() return RbI.account.settings.taskDelay end,
		setFunc = function(value) RbI.account.settings.taskDelay = value end,
		width = 'full',
		min = 0,
		max = 2000,
		step = 100,
		clampInput = true,
	})

	return settingsMenu
end
