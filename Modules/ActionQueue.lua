-----------------------------------------------------------------------------------
-- Addon Name: Rulebased Inventory
-- Creator: TaxTalis
-- Addon Ideal: user-defined rule-based actions for inventory application
-- Addon Creation Date: September 11, 2018
--
-- File Name: Queue.lua
-- File Description: This file contains the queue on which actions of tasks are stored to be executed in a more kick-free way
-- Load Order Requirements: before Action
-- 
-----------------------------------------------------------------------------------

-- imports
local RbI = RulebasedInventory
local ruleEnv = RbI.ruleEnvironment
local utils = RbI.utils
local ctx = RbI.executionEnv.ctx

local function FlushDeconstruct()
	-- due to a eso-bug it wants to play creations sounds. Init them here to 'none'
	if(ctx.station == CRAFTING_TYPE_ENCHANTING) then
		ENCHANTING.potencySound = SOUNDS["NONE"]
		ENCHANTING.potencyLength = 0
		ENCHANTING.essenceSound = SOUNDS["NONE"]
		ENCHANTING.essenceLength = 0
		ENCHANTING.aspectSound = SOUNDS["NONE"]
		ENCHANTING.aspectLength = 0
	end
	SendDeconstructMessage()
end

local function NewActionQueue(instant)
	local self = {
		recallRefine = false,
		output = {},
		messageSize = 0
	}

	local function Output()
		for i, taskName in ipairs(self.output) do
			self.output[i] = nil
			RbI.PrintOutput('Task ' .. taskName)
			self.output[taskName] = false
		end
	end
	
	local asyncQueue
	asyncQueue = utils.NewAsyncQueue(instant, RbI.account.settings.actionDelay,
		function(entry) -- Next
			local taskName = entry.taskName
			local itemLink = entry.itemLink
			local amount = entry.amount
			local func = entry.func
			if(RbI.allowAction[taskName] or taskName == 'DestroyMove') then
				local isDeconstruct = taskName == 'Deconstruct' or taskName == 'Refine' or taskName == 'Extract'
				if(taskName ~= 'DestroyMove' and not instant) then
					-- d(taskName, itemLink, amount)
					-- d(".....")
					RbI.AddMessage('Task ' .. taskName, nil, taskName, false, nil, itemLink, amount)
					-- d(".....")
				end

				if(isDeconstruct and self.messageSize == 0) then
					PrepareDeconstructMessage()
					if(taskName == 'Refine') then
						self.recallRefine = true
					end
				end
				if(instant) then -- instant action messages
					local result, optMsg = func()
					if(type(result) == 'string') then
						RbI.out.msg("Usage of "..itemLink.." failed! ("..result..")")
					else
						if(result) then -- result==nil will be requeue
							RbI.out.msg("Used "..itemLink..(optMsg or ''))
						elseif(result == false) then
							RbI.out.msg("Usage of "..itemLink.." failed!")
						end
					end
				else
					func() -- execute queued action
				end

				if(isDeconstruct) then
					if(self.messageSize == 99) then
						FlushDeconstruct()
						self.messageSize = 0
						asyncQueue.Pause()
						EVENT_MANAGER:RegisterForEvent(RbI.name .. 'WaitForDeconstructMessageToBeRead', EVENT_CRAFT_COMPLETED, asyncQueue.Run)
					else
						self.messageSize = self.messageSize + 1
					end
				end
			else
				RbI.AddMessage('Task ' .. taskName, 'Final', taskName, false, 'canceled.')
			end
		end,
		nil, -- EqualsFn
		function() -- Finish
			EVENT_MANAGER:UnregisterForEvent(RbI.name .. 'WaitForDeconstructMessageToBeRead', EVENT_CRAFT_COMPLETED)
			if(RbITaskTestButton ~= nil and RbITaskRunButton ~= nil) then
				zo_callLater(function() RbITaskTestButton:UpdateDisabled() RbITaskRunButton:UpdateDisabled() end, 2000)
			end
			if(self.messageSize > 0) then
				FlushDeconstruct()
				self.messageSize = 0
			end
			if(self.recallRefine) then
				EVENT_MANAGER:RegisterForEvent(RbI.name .. 'WaitForDeconstructMessageToBeRead', EVENT_CRAFT_COMPLETED, function() RbI.TaskStarter(RbI.tasks.internal['Refine'], nil, 'REFINEMENT') asyncQueue.Run() end)
				self.recallRefine = false
				return
			end

			Output()
		end
	)

	local function Run(taskId, testrun, start, final)
		if(taskId ~= nil and not instant and taskId ~= 'DestroyMove') then
			if(start) then
				RbI.AddMessage('Task ' .. taskId, 'Start', taskId, testrun)
			end
			if(not self.output[taskId]) then
				self.output[taskId] = true
				table.insert(self.output, taskId)
			end
			RbI.AddMessage('Task ' .. taskId, 'Final', taskId, testrun, final)
		end
		asyncQueue.Run()
	end
	
	local function Push(taskName, itemLink, amount, func, delay)
		asyncQueue.Push({
			taskName = taskName,
			itemLink = itemLink,
			amount = amount,
			func = func
		}, delay)
	end

	return {
		Run = Run,
		Push = Push,
		AddFinishCallback = asyncQueue.AddFinishCallback,
		AddStartCallback = asyncQueue.AddStartCallback,
		RemoveStartCallback = asyncQueue.RemoveStartCallback,
		RemoveFinishCallback = asyncQueue.RemoveFinishCallback,
		IsRunning = function()
			return asyncQueue.IsRunning()
		end
	}
end

local actionQueue
local actionQueueInstant
local function getQueue(taskName)
	if(taskName == 'Use') then
		return actionQueueInstant
	else
		return actionQueue
	end
end
RbI.ActionQueue = {
	Run = function(taskName, testrun, start, final)
		return getQueue(taskName).Run(taskName, testrun, start, final)
	end,
	Push = function(taskName, itemLink, amount, func, delay)
		return getQueue(taskName).Push(taskName, itemLink, amount, func, delay)
	end,
	IsRunning = function(taskName)
		if(taskName == nil) then
			return actionQueue.IsRunning() or actionQueueInstant.IsRunning()
		end
		return getQueue(taskName).IsRunning()
	end,
	AddFinishCallback = function(taskName, callback)
		getQueue(taskName).AddFinishCallback(callback)
	end,
	AddStartCallback = function(taskName, callback)
		getQueue(taskName).AddStartCallback(callback)
	end,
	RemoveStartCallback = function(taskName, callback)
		getQueue(taskName).RemoveStartCallback(callback)
	end,
	RemoveFinishCallback = function(taskName, callback)
		getQueue(taskName).RemoveFinishCallback(callback)
	end,

}
RbI.addInitialize(function()
	actionQueue = NewActionQueue(false)
	actionQueueInstant = NewActionQueue(true)
end)