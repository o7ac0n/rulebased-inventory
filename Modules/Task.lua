-----------------------------------------------------------------------------------
-- Addon Name: Rulebased Inventory
-- Creator: TaxTalis
-- Addon Ideal: user-defined rule-based actions for inventory application
-- Addon Creation Date: September 11, 2018
--
-- File Name: Task.lua
-- File Description: This file contains the definition of tasks (e.g. BagToBank)
-- Load Order Requirements: after Rule, after RuleSet, before Profile, before Loader
-- 
-----------------------------------------------------------------------------------

-- imports
local RbI = RulebasedInventory
local ruleEnv = RbI.ruleEnvironment
local ctx = RbI.executionEnv.ctx
local utils = RbI.utils

RbI.tasks = RbI.tasks or {}
RbI.tasks.list = RbI.tasks.list or {} -- for menu dropdown names
RbI.tasks.internal = RbI.tasks.internal or {} -- all tasks are internal

local internalRules = {}
RbI.internalRules = internalRules
-- hard game preconditions that a task/action can be executed at all.
internalRules.BagToBank 	= 'not (stolen() or bindType("characterbound") or (unique() and countbank(">", 0)) or type("container"))'
internalRules.BankToBag 	= 'not (unique() and countbackpack(">", 0))'
internalRules.CraftToBag    = ''
internalRules.Deconstruct	= 'fcoispermission() and canDeconstruct() and not locked()'
internalRules.Destroy		= 'fcoispermission() and not locked()'
internalRules.Extract		= 'fcoispermission() and canExtract() and not locked()'
internalRules.Fence 		= 'fcoispermission() and stolen() and not locked()'
internalRules.Junk 			= 'fcoispermission() and not junked() and not locked() and not companion()' -- junking is not possible with ingame lock in place (though locked also returnes fcoisLock...)
internalRules.Launder 		= internalRules.Fence
internalRules.Notify 		= ''
internalRules.Refine		= 'fcoispermission() and canRefine() and not locked()'
internalRules.Sell 			= 'fcoispermission() and not stolen() and not locked() and sellinformation() ~= 4' -- 4 = ITEM_SELL_INFORMATION_CANNOT_SELL
-- not locked used for: Deconstruct, Destroy, Extract, Fence, Junk, Refine, Sell
internalRules.UnJunk 		= 'junked()'
internalRules.Use 		    = 'usable()'
internalRules.Safety		= 'not (\n  quality("legendary")\n  or trait("weapon_nirnhoned")\n  or price() > 10000\n  or crownCrate()\n)'
-- Safety rule is currently used for: Deconstruct, Destroy, Extract, Fence, Sell (no need for 'not locked()' because it's already in all internal task rules)
local taskDatas = {}
local name = function(name)
	return function() return name, name end
end
taskDatas[#taskDatas+1] = {name('BagToBank'), true, false, false, BAG_BACKPACK, 'MoveItem', BAG_BANK, false, 'BankToBag', 'Puts items from your backpack in your bank.'}
taskDatas[#taskDatas+1] = {name('BankToBag'), true, false, false, BAG_BANK, 'MoveItem', BAG_BACKPACK, false, nil, 'Puts items from your bank in your backpack'}
taskDatas[#taskDatas+1] = {name('CraftToBag'), true, true, false, BAG_VIRTUAL, 'MoveItem', BAG_BACKPACK, false, nil, 'Puts items from your craftbag into your backpack'}
taskDatas[#taskDatas+1] = {name('Deconstruct'), true, false, false, 'DECONSTRUCTION', 'DeconstructItem', nil, true, nil, 'Deconstructs items at crafting station'}
taskDatas[#taskDatas+1] = {name('Destroy'), true, true, false, BAG_BACKPACK, 'DestroyItem', nil, true, nil, 'Destroys items'}
taskDatas[#taskDatas+1] = {name('Extract'), true, false, false, 'EXTRACTION', 'DeconstructItem', nil, true, nil, 'Extracts items at enchanting crafting station'}
taskDatas[#taskDatas+1] = {name('Fence'), true, false, false, BAG_BACKPACK, 'FenceItem', nil, true, nil, 'Sells items at the fence-store'}  -- sell
taskDatas[#taskDatas+1] = {name('Junk'), true, true, true, BAG_BACKPACK, 'JunkItem', nil, false, nil, 'Marks items as junk'}
taskDatas[#taskDatas+1] = {name('Launder'), true, false, false, BAG_BACKPACK, 'LaunderItem', nil, false, nil, 'Launders the item, removing the stolen marker'} -- remove stolen marker
taskDatas[#taskDatas+1] = {name('Notify'), true, true, false, BAG_BACKPACK, 'Notify', nil, false, nil, 'Prints a notify message in the chat including price'}
taskDatas[#taskDatas+1] = {name('Refine'), true, false, false, 'REFINEMENT', 'DeconstructItem', nil, false, nil, 'Refines items at woodworking/blacksmithing/clothier crafting station'}
taskDatas[#taskDatas+1] = {name('Sell'), true, false, false, BAG_BACKPACK, 'SellItem', nil, true, nil, 'Sells items at vendor-store'}
taskDatas[#taskDatas+1] = {name('Use'), true, true, false, BAG_BACKPACK, 'UseItem', nil, false, nil, 'Uses the items'}

taskDatas[#taskDatas+1] = {name('UnJunk'), false, true, false, BAG_BACKPACK, 'UnJunkItem', nil, false, 'Junk'}
-- taskDatas['BagToCraft'] = {BAG_BACKPACK, 'MoveItem', BAG_VIRTUAL, false}


local function Task(nameFn, isForUser, defaultAllowAction, hasRevertTask, bagFrom, actionName, bagTo, useSafetyRule, blockerTaskId, description)
	local self = {}
	local id = nameFn()
	self.id = id
	self.blockerTaskId = blockerTaskId
	self.defaultAllowAction = defaultAllowAction
	self.bagFrom = bagFrom
	self.bagTo = bagTo
	self.Action = RbI.actions[actionName]
	self.internalRules = {id}
	self.alreadyCheckedItemLinks = {}
	self.description = description or false
	if(description ~= nil and internalRules[id]~=nil and internalRules[id]~='') then
		self.description = self.description..' \n\nInternal precondition:\n'..internalRules[id]
	end
	
	local function ResetAlreadyCheckedItemLinks()
		self.alreadyCheckedItemLinks = {}
	end

	local function GetDefaultAllowAction()
		return defaultAllowAction
	end
	
	local function IsEnabled()
		return self.RuleSet.HasRules()
	end
	
	local function GetBagFrom()
		return self.bagFrom
	end
	
	local function GetId()
		return self.id
	end

	local function IsUserTask()
		return isForUser
	end

	local function RunWithRevertTask()
		local taskSettings = RbI.account.tasks[GetId()]
		if(taskSettings) then
			return taskSettings.runWithRevertTask
		end
		return false
	end

	local function HasRevertTask()
		return hasRevertTask
	end

	local function GetOpposingTask()
		if(blockerTaskId) then
			return RbI.tasks.internal[blockerTaskId]
		else
			for _, task in pairs(RbI.tasks.internal) do
				local optBlockerTask = task.GetBlockerTask()
				if(optBlockerTask ~= nil and optBlockerTask.GetId() == self.id) then
					return task
				end
			end
		end
	end
	
	local function GetBlockerTask()
		if(blockerTaskId) then
			return RbI.tasks.internal[blockerTaskId]
		end
	end

	local function GetDisplayName()
		local _, displayName = nameFn()
		return displayName
	end

	-- add taskName to list for menu dropdown names
	-- as tasks are only created once, we can add them straigt on
	-- and we do not need functionality to delete them later
	if(isForUser) then
		table.insert(RbI.tasks.list, self.id)
	end

	-- @return status: boolean. 'false' if an error occured (from the task or a potential opposingTask)
	-- @return result: boolean. The result for this item.
	-- @return resultInv: boolean. The result of the opposingTask for this item.
	local function Evaluate(bag, slot, itemLink)
		local status = true
		local result = false

		ctx.item = {
			bag = bag,
			slot = slot,
			link = itemLink
		}
		-- Internal rules verification
		for i, ruleName in pairs(self.internalRules) do
			if(ruleName ~= 'Safety' or RbI.account.settings.safetyrule) then
				status, result = ruleEnv.RunRuleWithItem(RbI.rules.internal[ruleName], bag, slot, itemLink, GetDisplayName())
				if(status == false or result == false) then
					-- d("internal " .. ruleName .. " false")
					return status, result
				end
			end
		end
		
		local statusInv = true
		local resultInv = false
		if(blockerTaskId ~= nil) then
			statusInv, resultInv = RbI.RuleSet.Evaluate(blockerTaskId, bag, slot, itemLink)
		end
		if(statusInv) then
			status, result = RbI.RuleSet.Evaluate(self.id, bag, slot, itemLink)
		end
		return (status and statusInv), result, resultInv
	end
	
	-- result holds an amount to each bag/slot found in given slots which shall be taken action on
	-- GetSlotDatas' returned table (here child-table-of-tables: slotDatas) must not be written to!
	local function GetSlotFromDatas(slotDatas, bagTo, bagToCount)
		
		local slotFromDatas = {}
		-- we assume that with the masked itemLink used for sorting, 
		-- that the count returned by GetItemLinkStacks is true for all slotDatas
		local bagCount = {}
		for	bag, count in pairs(slotDatas.count) do
			bagCount[bag] = count
		end
		local status = true
		local itemLink, bagFrom
		
		local tmpSlotDatas = {}
		-- GetSlotDatas' returned table (here child-table-of-tables: slotDatas) must not be written to!
		-- clear! slotData is a table, which ipairs does a copy of, no linked tables inside slotData.
		local count = 0
		for i, slotData in ipairs(slotDatas) do
			table.insert(tmpSlotDatas, slotData)
			count = count + 1
		end
		
		local index = nil
		while(count > 0) do
			for i, slotData in pairs(tmpSlotDatas) do
				if(index == nil or tmpSlotDatas[index].stack > slotData.stack) then
					index = i
				end
			end
			
			-- d(index)
			
			local slotData = tmpSlotDatas[index]
			tmpSlotDatas[index] = nil
			index = nil
			count = count - 1
			
			bagFrom = slotData.bag
			if(bagFrom == BAG_SUBSCRIBER_BANK) then
				bagFrom = BAG_BANK
			end
			
			ctx.amountHelper = {}
			ctx.amountHelper.count = {}
			ctx.amountHelper.count[bagFrom] = bagCount[bagFrom]
			if(bagTo ~= nil) then
				ctx.amountHelper.count[bagTo] = bagToCount
			end
			
			local resultSelf, resultInv
			status, resultSelf, resultInv = Evaluate(slotData.bag, slotData.slot, slotData.itemLink)
			
			
			if(status) then
				if(resultSelf) then	
					local remainingCount = bagCount[bagFrom]
					local amountHelpers = {}
					for amountHelper in pairs(ctx.amountHelper[bagFrom] or {}) do
						amountHelpers[amountHelper] = true
					end
					amountHelpers[0] = true --try moving all
					if(bagTo ~= nil) then
						for amountHelper in pairs(ctx.amountHelper[bagTo] or {}) do
							if(0 <= amountHelper and amountHelper >= bagToCount) then
								amountHelpers[bagCount[bagFrom] + (bagToCount - amountHelper)] = true
							end
						end
					end
					
					
					
					-- local cnt = 0
					local amountHelper = table.maxn(amountHelpers)
					while (true) do
						-- d("-----AMOUNT-------")
						-- d(amountHelper)
						-- d(amountHelpers)
						-- d("------------")
						amountHelpers[amountHelper] = nil	
						if(amountHelper <= remainingCount) then	
							ctx.amountHelper.count = {}
							ctx.amountHelper.count[bagFrom] = amountHelper
							if(bagTo ~= nil) then
								ctx.amountHelper.count[bagTo] = bagToCount + (bagCount[bagFrom] - amountHelper)
							end
							status, resultSelf, resultInv = Evaluate(slotData.bag, slotData.slot, slotData.itemLink)
							if(status) then
								if(resultSelf) then
									-- rule tells to do something
									if(resultInv) then
										-- invert tells opposide side would do something
										-- d("(1) remainingCount set: " .. remainingCount)
										break
									else
										-- inverted has nothing against this move
										remainingCount = amountHelper
										-- d("(2) remainingCount set: " .. remainingCount)
									end
								else 
									-- this is the first time rule has failed, 
									-- then this is the remainingCount we want to keep
									-- if not the other side would do something
									
									if(resultInv) then
										-- invert tells opposide side would do something
										-- d("(3) remainingCount set: " .. remainingCount)
									else
										-- inverted has nothing against this move
										remainingCount = amountHelper
										-- d("(4) remainingCount set: " .. remainingCount)
									end
									
									break
								end
							else
								-- an error occured in a rule
								break
							end
						end
						if(amountHelper == 0) then
							-- d("????????????????????????????????????")
							break
						end
						amountHelper = table.maxn(amountHelpers)
						-- cnt = cnt + 1
						-- if(cnt == 10) then
							-- d("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
						-- end
					end
					-- d("+++++++++++++++")
					if(remainingCount < bagCount[bagFrom]) then
						-- slotData is a table, which ipairs does a copy of, no linked tables inside slotData.
						slotData.stackCountChange = -math.min((bagCount[bagFrom] - remainingCount), slotData.stack)
						if(bagTo ~= nil) then
							bagToCount = bagToCount - slotData.stackCountChange
						end
						bagCount[bagFrom] = bagCount[bagFrom] + slotData.stackCountChange
						table.insert(slotFromDatas, slotData)
						-- d("counts")
						-- d(bagCount[bagFrom])
						-- d(bagToCount)
						-- d("......")
					end
				end	
			else
				-- an error occured in a rule
				break
			end
		end
		-- d("#####")
		-- d(slotFromDatas)
		-- slotFromDatas = {}
		return status, slotFromDatas
	end
	
	local function Finish(singleItemLink, status, finished)
		local start, final = false
		if(singleItemLink == nil) then
			if(status) then
				if(finished ~= nil or RbI.testrun) then
					start = true
					if(finished == nil or finished) then
						final = 'finished'
					else
						if(self.id ~= 'Fence' and self.id ~= 'Launder') then
							final = 'halted: Target Bag is full!'
						else
							final = 'halted: Daily Limit reached!'
						end
					end
				end
			else
				final = 'canceled: An error ocurred!'
			end
		end
		RbI.ActionQueue.Run(self.id, RbI.testrun, start, final)
	end
	
	local function CheckByItemLink(singleItemLink, bagFrom, bagTo)
		local status = true
		local finished = nil
		local actionCount = 0

		local bagFrom = bagFrom or self.bagFrom
		local bagTo = bagTo or self.bagTo
		-- just so, when needed later on, the lookup will not be cleared before this task has finished
		local lookupBagFrom = RbI.GetLookup(bagFrom, true)
		local lookupBagTo = RbI.GetLookup(bagTo, true)

		if(RbI.RuleSet.HasRules(self.id)) then
			finished = true
			-- GetSlotDatas' returned table must not be written to!
			local bagFromOccupied = lookupBagFrom.GetSlotDatas(singleItemLink)
			local bagToOccupied = {}
			if(lookupBagTo ~= nil) then 
				bagToOccupied = lookupBagTo.GetSlotDatas(singleItemLink)
			end

			-- slotDatas is still linked to bagFromOccupied, this potentially has <unforseen consequences>
			-- GetSlotDatas' returned table (here child-table-of-tables: slotDatas) must not be written to!
			for itemLinkForSorting, slotDatas in pairs(bagFromOccupied) do
				-- d(self.id)
				-- slotDatas.count can be nil when the craftBag sucks the item after laundering out of the bag
				if(self.alreadyCheckedItemLinks[itemLinkForSorting] == nil and slotDatas.count~=nil) then
					local bagToCount = {}
					if(bagToOccupied[itemLinkForSorting] ~= nil) then
						-- d('bagToCalc')
						-- d(bagToOccupied[itemLinkForSorting])
						bagToCount = bagToOccupied[itemLinkForSorting].count or {}
					end
					bagToCount = bagToCount[bagTo] or 0
					-- GetSlotDatas' returned table (here child-table-of-tables: slotDatas) must not be written to!
					local slotFromDatas
					status, slotFromDatas = GetSlotFromDatas(slotDatas, bagTo, bagToCount)
					if(status) then
						-- d("1+++++" .. itemLinkForSorting)
						-- d(slotFromDatas)
						-- d("2+++++" .. itemLinkForSorting)
						-- d(slotDatas)
						-- d("3+++++" .. itemLinkForSorting)
						-- d(bagFromOccupied[itemLinkForSorting])
						-- d("4+++++" .. itemLinkForSorting)
						for i, slotFromData in pairs(slotFromDatas) do
							local authorizedAmount = -slotFromData.stackCountChange
							if(RbI.testrun == false) then
								local remainingAmount = self.Action(self.id, itemLinkForSorting, slotFromData, bagFrom, bagTo)
								if(remainingAmount < authorizedAmount) then
									--d(self.id .. " executed " .. (authorizedAmount - remainingAmount) .. 'x ' .. slotFromData.itemLink)
									actionCount = actionCount + 1
								end
								if(remainingAmount > 0 and (remainingAmount > 10 or self.id ~= 'Refine')) then
									-- bagTo is full, suspend this task, look for other task
									--d(self.id .. " bagTo is full...")
									finished = false
									break
								else
									-- if itemLink has been successful checked, skip it next time if self call happens
									self.alreadyCheckedItemLinks[itemLinkForSorting] = true
								end
							else
								RbI.AddMessage('Task ' .. self.id, nil, self.id, RbI.testrun, nil, slotFromData.itemLink, authorizedAmount)
							end
						end
					else
						-- an error occured in a rule
						break
					end
				end
			end
		else
			actionCount = 0
		end
		-- clear up
		RbI.ClearLookup(bagFrom)
		RbI.ClearLookup(bagTo)
		Finish(singleItemLink, status, finished)
		return status, finished, actionCount
	end

	-- bagFrom (optional), only considered and needed for tasks which do not provide their own
	-- singleSlotFrom(optional), leave blank to scan/execute on complete bag
	-- stackCountChange (optional), for Notify to show how many items were found
	local function CheckBySlot(singleSlotFrom, bagFrom, stackCountChange)
		local bagFrom = bagFrom or self.bagFrom
		local bagTo = self.bagTo
		local status, result = true, false
		local actionCount = 0
		if(RbI.RuleSet.HasRules(self.id)) then
			-- slotFrom begins by 0
			for slotFrom = singleSlotFrom or 0, singleSlotFrom or GetBagSize(bagFrom) - 1 do
				if(HasItemInSlot(bagFrom, slotFrom)) then
					local itemLink = GetItemLink(bagFrom, slotFrom)
					local itemLinkForSorting = RbI.GetItemLinkForSorting(itemLink)
					status, result = Evaluate(bagFrom, slotFrom, itemLink, taskName)
					if(status) then
						if(result) then
							stackCountChange = -stackCountChange or -GetSlotStackSize(bagFrom, slotFrom)
							local slotFromData = {stackCountChange = stackCountChange, itemLink = itemLink, bag = bagFrom, slot = slotFrom}
							self.Action(self.id, itemLinkForSorting, slotFromData, bagFrom, bagTo)
							actionCount = actionCount + 1
						end
					else
						break
					end
				end
			end
		end
		Finish(singleSlotFrom, status, finished)
	end

	if(useSafetyRule) then
		table.insert(self.internalRules, 'Safety')
	end

	local task = {GetId = GetId
								,GetDisplayName = GetDisplayName
								,GetDefaultAllowAction = GetDefaultAllowAction
								,GetOpposingTask = GetOpposingTask
								,GetBlockerTask = GetBlockerTask
								,IsUserTask = IsUserTask
							   	,Evaluate = Evaluate
							   	,GetBagFrom = GetBagFrom
							   	,CheckBySlot = CheckBySlot
							   	,CheckByItemLink = CheckByItemLink
							   	,ResetAlreadyCheckedItemLinks = ResetAlreadyCheckedItemLinks
							   	,GetDescription = function() return self.description end
							   	,RunWithRevertTask = RunWithRevertTask
								,HasRevertTask = HasRevertTask
	 }
	RbI.tasks.internal[id] = task
	return task
end

RbI.taskMigrations = {}
function RbI.GenerateTasks()
	if(FCOIS) then
		local languageData = {}
		RbI.collectedData["fcoisMark"] = languageData
		-- local numFilterIcons = FCOIS.numVars.gFCONumFilterIcons
		local maxIconNr = FCOIS.mappingVars.dynamicToIcon[FCOIS.settingsVars.settings.numMaxDynamicIconsUsable]
		for iconNr=FCOIS_CON_ICON_LOCK, maxIconNr do
			local iconId = RbI.dataTypeStatic["fcoisMarkIds"][iconNr]
			RbI.RenameTask('FCOISMark:'..iconNr, 'FCOISMark:'..iconId) -- will be copied only if it exists todo: remove in a further version
			languageData[RbI.fcois.getNameByNr(iconNr)] = iconNr
			local taskId = 'FCOISMark:'..iconId
			local taskIdOpp = 'FCOISUnmark:'..iconId
			internalRules[taskId] = 'not fcoismarker('.."'"..iconId.."'"..')'
			taskDatas[#taskDatas+1] = {function() return taskId, 'FCOISMark: '..RbI.fcois.getNameByNr(iconNr) end, true, true, true, BAG_BACKPACK, 'FCOIS_MarkItem', nil, false, nil, 'Marks items using FCO ItemSaver addon. FCOIS provides itself also an automatic marking functionality. Please ensure that there are no contradictions.' }

			internalRules[taskIdOpp] = 'fcoismarker('.."'"..iconId.."'"..')'
			taskDatas[#taskDatas+1] = {function() return taskIdOpp, 'FCOISUnmark: '..RbI.fcois.getNameByNr(iconNr) end, false, true, false, BAG_BACKPACK, 'FCOIS_UnmarkItem', nil, false, taskId, 'Unmarks items using FCO ItemSaver addon'}
		end
		-- merge to dataType
		local targetEnumDataType = RbI.dataType["fcoisMark"]
		for key, value in pairs(languageData) do
			local loweredKey = key:lower()
			if(not targetEnumDataType[loweredKey]) then
				RbI.dataTypeHandler.addData(targetEnumDataType, loweredKey, value)
			end
		end
	end

	ruleEnv.RegisterInternalRule('Safety', internalRules['Safety'])

	for _, taskData in pairs(taskDatas) do
		local id, displayName = taskData[1]()
		ruleEnv.RegisterInternalRule(id, internalRules[id])
		local task = Task(unpack(taskData))
		RbI.allowAction[id] = task.GetDefaultAllowAction()
	end
end

local function IsInItemEventQueue(executeTaskArguments)
  return executeTaskArguments[1] ~= nil
end

local function TasksFinished(ExecuteTask, executeTaskArguments, executeTaskLastResults, InterleaveTask, interleaveTaskArguments, interleaveTaskLastResults)
	RbI.ClearLookup(ExecuteTask.GetBagFrom() or executeTaskArguments[1]) --bagFrom
	if(InterleaveTask ~= nil) then
	RbI.ClearLookup(InterleaveTask.GetBagFrom() or interleaveTaskArguments[1]) --bagFrom
	end

	ExecuteTask.ResetAlreadyCheckedItemLinks()
	if(InterleaveTask ~= nil) then
		InterleaveTask.ResetAlreadyCheckedItemLinks()
	end

	ctx.task = nil

	if(RbI.testrun) then
		zo_callLater(function() RbI.testrun = false RbITaskTestButton:UpdateDisabled() RbITaskRunButton:UpdateDisabled() end, 2000)
	end
end

local function Interleave(ExecuteTask, executeTaskArguments, executeTaskLastResults, InterleaveTask, interleaveTaskArguments, interleaveTaskLastResults)
	-- executing task cannot have finished == true as it would not have been called again
	-- d(executeTaskArguments, executeTaskLastResults, interleaveTaskArguments, interleaveTaskLastResults)

	if(executeTaskLastResults.status and interleaveTaskLastResults.status) then
		-- status of both tasks is ok
		if(executeTaskLastResults.actionCount == nil or interleaveTaskLastResults.actionCount > 0) then
			-- this task has not yet been run, or the last task has done something
			local s, f, a = ExecuteTask.CheckByItemLink(executeTaskArguments[1], executeTaskArguments[2], executeTaskArguments[3])
			executeTaskLastResults.status = s
			executeTaskLastResults.finished = f
			executeTaskLastResults.actionCount = a
			executeTaskLastResults.totalActionCount = executeTaskLastResults.totalActionCount + a

			if(interleaveTaskLastResults.finished == false) then
				-- interleaved did not finish, call it again
				Interleave(InterleaveTask, interleaveTaskArguments, interleaveTaskLastResults, ExecuteTask, executeTaskArguments, executeTaskLastResults)
			else
				-- interleaved task already did finish, no need for calling it again, selfcall just finished, everything possible should be done by now
				TasksFinished(ExecuteTask, executeTaskArguments, executeTaskLastResults, InterleaveTask, interleaveTaskArguments, interleaveTaskLastResults)
			end
		else
			-- last task did not finish but could not process any items, abort execution, there are no slots left
			TasksFinished(ExecuteTask, executeTaskArguments, executeTaskLastResults, InterleaveTask, interleaveTaskArguments, interleaveTaskLastResults)
		end
	else
		-- an error occured during execution, abort execution
		TasksFinished(ExecuteTask, executeTaskArguments, executeTaskLastResults, InterleaveTask, interleaveTaskArguments, interleaveTaskLastResults)
	end
end

function RbI.TaskStarter(ExecuteTask, ExecuteTaskItemLink, ExecuteTaskBagFrom, ExecuteTaskBagTo, InterleaveTask, InterleaveTaskItemLink, InterleaveTaskBagFrom, InterleaveTaskBagTo)
	if(not RbI.testrun and (not RbI.allowAction[ExecuteTask.GetId()] or (InterleaveTask ~= nil and not RbI.allowAction[InterleaveTask.GetId()]))) then
		return 0
	end
	ctx.task = {}
	local executeTaskArguments = {[1] = ExecuteTaskItemLink, [2] = ExecuteTaskBagFrom, [3] = ExecuteTaskBagTo}
	local interleaveTaskArguments = {[1] = InterleaveTaskItemLink, [2] = InterleaveTaskBagFrom, [3] = InterleaveTaskBagTo}
	local executeTaskLastResults = {status = true, finished = false, actionCount = nil, totalActionCount = 0}
	local interleaveTaskLastResults = {status = true, finished = false, actionCount = nil, totalActionCount = 0}

	RbI.GetLookup(ExecuteTask.GetBagFrom() or executeTaskArguments[1], true) --bagFrom
	if(InterleaveTask == nil) then
		interleaveTaskLastResults.finished = true
	else
		RbI.GetLookup(InterleaveTask.GetBagFrom() or interleaveTaskArguments[1], true) --bagFrom
	end
	
	Interleave(ExecuteTask, executeTaskArguments, executeTaskLastResults, InterleaveTask, interleaveTaskArguments, interleaveTaskLastResults)
	if(InterleaveTask == nil) then
		return executeTaskLastResults.actionCount
	end
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Single item backpack check ------------------------------------------------
------------------------------------------------------------------------------

local function checkItem(task, optItemLink, from, to)
	local status, finished, actionCount = task.CheckByItemLink(optItemLink, from, to)
	task.ResetAlreadyCheckedItemLinks()
	return actionCount
end

local function taskStarterFCOIS(optItemLink)
	for taskId, task in pairs(RbI.tasks.internal) do
		if(utils.startsWith(taskId, 'FCOISMark')) then
			checkItem(task, optItemLink, BAG_BACKPACK)
			local oppTask
			if(task.RunWithRevertTask() and RbI.RuleSet.HasRules(taskId)) then
				checkItem(task.GetOpposingTask(), optItemLink, BAG_BACKPACK)
			end
		end
	end
end

function RbI.CheckItemInBackpack(itemLink)
	RbI.GetLookup(BAG_BACKPACK, true)
	taskStarterFCOIS(itemLink)
	local junkTask = RbI.tasks.internal['Junk']
	local result = checkItem(junkTask, itemLink, BAG_BACKPACK)
	if(result == 0 and junkTask.RunWithRevertTask() and RbI.RuleSet.HasRules('Junk')) then
		result = checkItem(junkTask.GetOpposingTask(), itemLink, BAG_BACKPACK)
	end
	--d("JunkResult: "..tostring(result))
	if(result == 0) then
		result = checkItem(RbI.tasks.internal['Use'], itemLink, BAG_BACKPACK)
		--d("UseResult: "..tostring(result))
		if(result == 0) then
			result = checkItem(RbI.tasks.internal['Destroy'], itemLink, BAG_BACKPACK)
			--d("DestroyResult: "..tostring(result))
		end
	end
	RbI.ClearLookup(BAG_BACKPACK)
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Full item backpack check --------------------------------------------------
------------------------------------------------------------------------------

function RbI.RunTask(task, itemLink, finishFn)
	local added = false
	local startFn = function() added = true end
	local taskId = task.GetId()
	RbI.ActionQueue.AddStartCallback(taskId, startFn)
	RbI.ActionQueue.AddFinishCallback(taskId, finishFn) -- d("finish:"..taskId)
	RbI.TaskStarter(task, itemLink)
	if(not added) then -- finish directly, removing all callbacks
		RbI.ActionQueue.RemoveStartCallback(taskId, startFn)
		RbI.ActionQueue.RemoveFinishCallback(taskId, finishFn)
	end
end

-- is a bit slower than RunTask, but I don't know why :/
function RbI.RunTask2(task, itemLink, finishFn)
	if(RbI.TaskStarter(task, itemLink) > 0) then
		RbI.ActionQueue.AddFinishCallback(task.GetId(), finishFn) -- d("RunTaskEnd "..task.GetId())
	else
		finishFn()
	end
end

local function runAllTasks(tasks) -- one task after another
	local startTime = GetGameTimeMilliseconds()
	RbI.allowAction["SlotUpdate"] = false
	local curI = 1
	local next
	next = function()
		local curTask = tasks[curI]
		if(curTask) then
			curI = curI + 1
			RbI.RunTask(curTask, nil, next)
		else
			RbI.out.msg("Finished items check! "..((GetGameTimeMilliseconds()-startTime)/1000).." secs")
			RbI.allowAction["SlotUpdate"] = true
		end
	end
	next()
end

-- much faster than queueing all items
function RbI.TaskStarterCheckAllItems(bag, withOpposite)
	local tasks = {}
	local junkTask = RbI.tasks.internal['Junk']
	table.insert(tasks, junkTask)
	if(withOpposite and junkTask.RunWithRevertTask() and RbI.RuleSet.HasRules('Junk')) then table.insert(tasks, junkTask.GetOpposingTask()) end -- Unjunk
	table.insert(tasks, RbI.tasks.internal['Destroy'])
	for taskId, task in pairs(RbI.tasks.internal) do
		if(utils.startsWith(taskId, 'FCOISMark')) then
			table.insert(tasks, task)
			if(withOpposite and task.RunWithRevertTask() and RbI.RuleSet.HasRules(taskId)) then table.insert(tasks, task.GetOpposingTask()) end -- Unmark
		end
	end
	table.insert(tasks, RbI.tasks.internal['Use'])
	runAllTasks(tasks)
end
