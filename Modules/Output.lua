-----------------------------------------------------------------------------------
-- Addon Name: Rulebased Inventory
-- Creator: TaxTalis
-- Addon Ideal: user-defined rule-based actions for inventory application
-- Addon Creation Date: September 11, 2018
--
-- File Name: Output.lua
-- File Description: This file contains string helper function the functions to
--                   create and print feedback for the user about actions a task took
-- Load Order Requirements: before Task
-- 
-----------------------------------------------------------------------------------

local RbI = RulebasedInventory
local output = {}

-- imports
local utils = RbI.utils
local execEnv = RbI.executionEnv

local messages = {}	-- 1: TestRun, 2: Amount, 3: ItemLink, 4: Price, 5: ItemData
messages.Junk = 		"<<1>> Junked <<2>>x <<3>>"
messages.UnJunk = 		"<<1>> UnJunked <<2>>x <<3>>"
messages.Launder = 		"<<1>> Laundered <<2>>x <<3>>"
messages.Destroy = 		"<<1>> Destroyed <<2>>x <<3>>"
messages.Deconstruct = 	"<<1>> Deconstructed <<2>>x <<3>>"
messages.Extract = 		"<<1>> Extracted <<2>>x <<3>>"
messages.Refine = 		"<<1>> Refined <<2>>x <<3>>"
messages.BagToBank = 	"<<1>> Stored <<2>>x <<3>> <<5>>"
messages.BankToBag = 	"<<1>> Received <<2>>x <<3>> <<5>>"
messages.Notify = 		"<<1>> Found <<2>>x <<3>> <<5>>"
messages.Sell = 		"<<1>> Sold <<2>>x <<3>> for <<4>>"
messages.Fence = 		"<<1>> Fenced <<2>>x <<3>> for <<4>>"
messages.CraftToBag = 	"<<1>> Fetched <<2>>x <<3>>"
messages.Use   = 		"<<1>> Used <<3>>"
local fcoisMark = "FCOISMark"
messages[fcoisMark] =   "<<1>> Marked <<3>> <<6>>"
local fcoisUnmark = "FCOISUnmark"
messages[fcoisUnmark] =   "<<1>> Unmarked <<3>> <<6>>"

messages.RuleTest = 	"<<1>> Rule applies to <<3>>"

messages.Summary = 		"<<1>> Total: <<2>> items for <<4>>"
messages.Final = 		"<<1>> <<2>> <<3>>"
messages.Start = 		"<<1>> <<2>> <<3>>"

-- start/final: testrun, task/ruleName, message
function RbI.PrintMessage(message, testrun, amount, itemLink, price, itemData, additional)
	local test = ''
	if(testrun) then 
		test = 'TEST: '
	end
	if(price ~= nil) then
		price = ZO_Currency_FormatPlatform(CURT_MONEY, price, ZO_CURRENCY_FORMAT_AMOUNT_ICON)
	end
	RbI.out.msg(message
		,test
		,amount
		,itemLink
		,price
		,itemData
		,additional)
end

function RbI.AddMessage(queueName, messageType, messageName, testrun, message, itemLink, amount)
	if(messageName ~= nil) then
		if(utils.startsWith(messageName, fcoisMark)) then
			messageName = fcoisMark
		elseif(utils.startsWith(messageName, fcoisUnmark)) then
			messageName = fcoisUnmark
		end
	end
	-- d(queuename, messagetype, messagename, testrun, message, itemlink, amount)
	-- d("######################")
	output[queueName] = output[queueName] or {}
	if(messageType == 'Start') then
		if(not output[queueName].start) then
			output[queueName].start = true
			if((testrun or RbI.account.settings.taskStartOutput) and RbI.allowAction["SlotUpdate"]) then
				RbI.PrintMessage(messages.Start, testrun, queueName, 'started...')
			end
		end
	elseif(messageType == 'Final') then
		output[queueName].final = message
	else
		output[queueName].messageName = messageName
		if(amount ~= nil) then
			output[queueName][itemLink] = (output[queueName][itemLink] or 0) + amount
		else
			output[queueName][itemLink] = true
		end
	end
	output[queueName].testrun = testrun
end

function RbI.PrintOutput(queueName)
	if(output[queueName] ~= nil) then
		local testrun = output[queueName].testrun
		local finalMessage = output[queueName].final
		local messageName = output[queueName].messageName
		output[queueName].final = nil
		output[queueName].start = nil
		output[queueName].testrun = nil
		output[queueName].messageName = nil
		
		local sold = 0 -- sum of value
		local total = 0 -- sum of amount
		for itemLink, amount in pairs(output[queueName] or {}) do
			local sum = 0
			if(messageName ~= 'RuleTest') then
				sum = GetItemLinkValue(itemLink) * amount
			end
			if(RbI.account.settings.taskActionOutput or testrun) then
				local itemData = ""
				if(messageName ~= 'RuleTest') then
					local fontsize = CHAT_SYSTEM.GetFontSizeFromSetting()
					-- show vouchers on all messages which are designed to show additional data
					local vouchers = execEnv.vouchers(itemLink)
					if(vouchers > 0) then
						if(itemData ~= "") then
							itemData = itemData ..", "
						end
						itemData = itemData .. ZO_Currency_FormatPlatform(CURT_WRIT_VOUCHERS, vouchers, ZO_CURRENCY_FORMAT_AMOUNT_ICON)
					end
					
					local backpack, bank, craftbag = GetItemLinkStacks(itemLink)
					if(craftbag > 0) then
						if(itemData ~= "") then
							itemData = itemData ..", "
						end
						itemData = itemData .. craftbag .. zo_iconTextFormat("EsoUI/Art/Tooltips/icon_craft_bag.dds", fontsize, fontsize)
					end
					if(bank > 0) then
						if(itemData ~= "") then
							itemData = itemData ..", "
						end
						itemData = itemData .. bank .. zo_iconTextFormat("EsoUI/Art/Tooltips/icon_bank.dds", fontsize, fontsize)
					end
					if(backpack > 0) then
						if(itemData ~= "") then
							itemData = itemData ..", "
						end
						itemData = itemData .. backpack .. zo_iconTextFormat("EsoUI/Art/Tooltips/icon_bag.dds", fontsize, fontsize)
					end

					-- show extendet data only on notify message
					if(messageName == "Notify") then
						local price = execEnv.price(itemLink)
						if(price > 0) then
							if(itemData ~= "") then
								itemData = itemData ..", "
							end
							itemData = itemData .. ZO_Currency_FormatPlatform(CURT_MONEY, price, ZO_CURRENCY_FORMAT_AMOUNT_ICON)
						end
					end

					if(itemData ~= "") then
						itemData = "(" .. itemData ..")"
					end
				end
				local additional = ""
				if(utils.startsWith(messageName, fcoisMark) or utils.startsWith(messageName, fcoisUnmark)) then
					local iconId = queueName:sub(queueName:find(":")+1)
					additional = " with "..RbI.fcois.getNameById(iconId)
				end
				local message = messages[messageName]
				if(message == nil) then error("Message for '"..messageName.."' can't be found!") end
				RbI.PrintMessage(message, testrun, amount, itemLink, sum, itemData, additional)
			end
			if(messageName ~= 'RuleTest') then
				total = total + amount
				sold = sold + sum
			end
			output[queueName][itemLink] = nil
		end
		
		if(RbI.account.settings.taskSummaryOutput and (messageName == 'Sell' or messageName == 'Fence') and total > 0) then
			RbI.PrintMessage(messages.Summary, testrun, total, nil, sold)
		end
		
		if((testrun or RbI.account.settings.taskFinishOutput) and finalMessage ~= nil and RbI.allowAction["SlotUpdate"]) then
			RbI.PrintMessage(messages.Final, testrun, queueName, finalMessage)
		end
		
		output[queueName] = nil
	end
end

RbI.out = {
	msg = function(msg, ...)
		local colorTag = "|c"..RbI.account.settings.outputcolor
		msg = "[RbI] "..zo_strformat(msg, ...)
		msg = msg:gsub("|c", "|#|c"):gsub("|r", "|r"..colorTag):gsub("|#", "|r")
		CHAT_ROUTER:AddSystemMessage(colorTag..msg.."|r")
	end,
	notify = function(amount, itemLink)
		local price = execEnv.price(itemLink)
		local itemData = ""
		if(price > 0) then
			if(itemData ~= "") then
				itemData = itemData ..", "
			end
			itemData = itemData .. ZO_Currency_FormatPlatform(CURT_MONEY, price, ZO_CURRENCY_FORMAT_AMOUNT_ICON)
		end
		if(itemData ~= "") then
			itemData = "(" .. itemData ..")"
		end
		RbI.PrintMessage(messages.Notify, false, amount, itemLink, price, itemData)
	end
}

-- Default error handling
function RbI.RuleErrorHandler(rule, err)
	local m = {}
	if(err.ruleName ~= nil) then
		m[#m + 1] = 'An error occured in rule ' .. err.ruleName .. ':'
	end
	if (err.message == nil) then -- this includes type(err) == 'string'
		m[#m + 1] = '>>> Unexpected RbI error in version '..RbI.addonVersion..' <<< Please report this to:\n'..RbI.websiteComments..' or\n'..RbI.discord
	else
		m[#m + 1] = '|cff0000'..err.message..'|r'
	end
	if(err.ruleContent ~= nil) then
		m[#m + 1] = '\nRulecontent:'
		m[#m + 1] = err.ruleContent
	end
	if(err.stackTrace ~= nil and #err.stackTrace>0) then
		m[#m + 1] = "\nStacktrace:"
		if(rule ~= nil) then
			m[#m + 1] = "Rule: "..rule.GetName() -- initiator
		end
		for _, trace in pairs(err.stackTrace) do
			-- unpack table argument like 'defaultchars'
			local traceArguments = trace[2]
			if(#traceArguments>0 and type(traceArguments[1])=="table") then
				traceArguments = traceArguments[1]
			end
			m[#m + 1] = "=> "..trace[1].."("..RbI.utils.ArrayValuesToString(traceArguments)..")"
		end
	end
	if (err.addition) then
		m[#m + 1] = "\n"..err.addition
	end
	RbI.PrintToClipBoard(table.concat(m, '\n'), 'Rule Inspector')
end

-- static key
function RbI.dataKeyFixed(str)
	if(str == nil) then return end
	return "|c03f8fc"..str.."|r"
end

-- language dependent data key
function RbI.dataKeyLD(str)
	if(str == nil) then return end
	return "|cfcba03"..str.."|r"
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Clipboard -----------------------------------------------------------------
------------------------------------------------------------------------------

local clipBoardWindow
local clipBoardTextBox
local clipBoardTitle
local clipBoardFooter

function RbI.InitializeOutput()
    clipBoardWindow = RulebasedInventory_Clipboard
	clipBoardTitle = RulebasedInventory_ClipboardTitle
	clipBoardFooter = RulebasedInventory_ClipboardFooter
    clipBoardTextBox = RulebasedInventory_ClipboardOutputBox
    clipBoardTextBox:SetMaxInputChars(100000)
	clipBoardTextBox:SetAllowMarkupType(ALLOW_MARKUP_TYPE_COLOR_ONLY) -- ALLOW_MARKUP_TYPE_COLOR_ONLY, ALLOW_MARKUP_TYPE_NONE
    clipBoardTextBox:SetHandler("OnFocusLost", function()
        clipBoardWindow:SetHidden(true)
    end)
end

local lpad = function(str, len, char)
	return str .. '\n' .. string.rep(char or ' ', len - #str - 1)
end

function RbI.PrintToClipBoard(text, title, footer)
	-- sometimes the clipboard will not show any content, when using ALLOW_MARKUP_TYPE_ALL
    -- with enough normal characters
	clipBoardTitle:SetText(title or "")
	clipBoardFooter:SetText(footer or "(To close: Press Escape or click outside the window)")
	clipBoardTextBox:SetText(lpad(text, text:len()+1000))
	clipBoardWindow:SetHidden(false)
	clipBoardTextBox:SetCursorPosition(0) -- scroll to top
    clipBoardTextBox:TakeFocus()
end
