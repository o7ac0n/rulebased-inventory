-----------------------------------------------------------------------------------
-- Addon Name: Rulebased Inventory
-- Creator: TaxTalis
-- Addon Ideal: user-defined rule-based actions for inventory application
-- Addon Creation Date: September 11, 2018
--
-- File Name: Item.lua
-- File Description: This file contains function-factories 
-- 					 and functions which are needed for rule evaluation
-- Load Order Requirements: before Rule, after RuleEnvironment, Utilities, any Extensions
-- 
-----------------------------------------------------------------------------------

-- Imports
local RbI = RulebasedInventory
local ruleEnv = RbI.ruleEnvironment
local self = RbI.executionEnv
local ctx = self.ctx
local ext = RbI.extensions
local utils = RbI.utils
local dataTypeHandler = RbI.dataTypeHandler

local NO_TYPE = nil
------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Helper functions ---------------------------------------------------------
------------------------------------------------------------------------------

-- Fix for monstersets which return 67 but should return 26
function RbI.GetItemLinkItemStyleFix(itemLink)
	local style = GetItemLinkItemStyle(itemLink)
	if(style == 67) then
		style = 26
	end
	return style
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Functions -----------------------------------------------------------------
------------------------------------------------------------------------------

local function ReturnItemDataValueAll(ItemDataFunction, inputType)
	local itemData
	if(inputType == 1) then
		return {ItemDataFunction(ctx.itemLink)}
	else
		return {ItemDataFunction(ctx.bag, ctx.slot)}
	end
end

local function ReturnItemDataValue(ItemDataFunction, itemDataIndex, inputType)
	return ReturnItemDataValueAll(ItemDataFunction, inputType)[itemDataIndex]
end

local function CheckItemDataByTable(ItemDataFunction, itemDataIndex, name, inputType, ...)
	local input = {...}

	local itemData
	if(inputType == 1) then
		itemData = (({ItemDataFunction(ctx.itemLink)})[itemDataIndex])
	else
		itemData = (({ItemDataFunction(ctx.bag, ctx.slot)})[itemDataIndex])
	end

	if(#input == 0) then
		return RbI.dataTypeHandler.GetKeysForValues(RbI.dataTypeStatic[name], itemData)
	end

	local data = RbI.dataType[name]
	for i = 1, #input do
		local inputI = string.lower(input[i])
		local curInputData = data[inputI]
		if (curInputData == nil) then
			ruleEnv.errorInvalidArgument2(i, inputI, name, name)
		elseif(dataTypeHandler.ContainsValue(curInputData, itemData)) then
			return true
		end
	end
	return false
end

local function CheckItemDataByMatrix(ItemDataFunction, name, inputType, ...)
	local input = {... }

	local itemDatas
	if(inputType == 1) then
		itemDatas = {ItemDataFunction(ctx.itemLink)}
	else
		itemDatas = {ItemDataFunction(ctx.bag, ctx.slot)}
	end

	if(#input == 0) then
		return RbI.dataTypeHandler.GetKeysForValues(RbI.dataTypeStatic[name], itemDatas)
	end

	local data = RbI.dataType[name]
	for i = 1, #input do
		local curInput = string.lower(input[i])
		local curInputData = data[curInput]
		if (curInputData == nil) then
			ruleEnv.errorInvalidArgument2(i, curInput, name, name)
		else
			for _, curItemData in pairs(itemDatas) do
				if(curInputData == curItemData) then
					return true
				end
			end
		end
	end
	return false
end

local function textCheck(input, itemDatas, withLibTextFilter)
	for i = 1, #input do
		for k = 1, #itemDatas do
			local inputI = string.lower(input[i])
			local itemData = tostring(itemDatas[k])
			if (inputI == itemData) then
				return true
			end
			if (withLibTextFilter and LibTextFilter ~= nil) then
				itemData = '$' .. itemData .. '$' --RbI's LibTextFilter Extention
				local result, status = LibTextFilter:Filter(itemData, inputI)
				if (status == LibTextFilter.RESULT_OK) then
					if (result) then
						return true
					end
				else
					local message
					if (status == LibTextFilter.RESULT_INVALID_ARGUMENT_COUNT) then
						message = 'An operator with too little arguments was encountered'
					elseif(status == LibTextFilter.RESULT_INVALID_VALUE_COUNT) then
						message = 'There where left over tokens that could not be matched'
					elseif(status == LibTextFilter.RESULT_INVALID_INPUT) then
						message = 'No string or a string with only operators was encountered'
					end
					ruleEnv.error(message)
				end
			end
		end
	end
	return false
end

local function CheckItemDataByValue(returnType, ItemDataFunction, itemDataIndex, inputType, ...)
	local input = {...}
	if(#input == 0) then
		return ReturnItemDataValue(ItemDataFunction, itemDataIndex, inputType)
	end

	local itemData
	if(inputType == 1) then
		itemData = ({ItemDataFunction(ctx.itemLink)})[itemDataIndex]
	else
		itemData = ({ItemDataFunction(ctx.bag, ctx.slot)})[itemDataIndex]
	end
	if(returnType == ruleEnv.GET_STRING) then
		itemData = string.lower(zo_strformat('<<1>>', itemData))
	end

	return textCheck(input, {itemData}, returnType == ruleEnv.GET_STRING)
end

local function GetItemLinkSetInfoSetName(itemLink)
	local _, name = GetItemLinkSetInfo(itemLink)
	return zo_strformat("<<1>>", name)
end

-- function name, ingame data retrieval function, index of return value to look at, rbi compare function, 1=itemlink / 2=bag+slot
-- rbi compare functions:
-- 'value' check for names (any match). Also uses LibTextFilter, when installed.
-- 'table' at least one filter argument (any match) which has to be included in the 'data'-domain, otherwise throws ruleEnv.errorInvalidArgument
-- 'return' direct return for numbers and booleans without any arguments
-- 'matrix' like 'table' but checks all values instead of a specific index
local itemFunctions = {}
itemFunctions.armorType		= {GetItemLinkArmorType, 						1, 'table', 	1}
itemFunctions.bag			= {function(itemLink) return ctx.bag end, 		1, 'table', 	1}
itemFunctions.bindType 		= {GetItemLinkBindType, 						1, 'table', 	1}
itemFunctions.bound 		= {IsItemLinkBound, 							1, 'return', 	1, ruleEnv.GET_BOOL }
itemFunctions.countStack 	= {GetSlotStackSize, 							1, 'return', 	2} -- stored by lookup
itemFunctions.countStackMax = {GetSlotStackSize, 							2, 'return', 	2} -- stored by lookup
itemFunctions.craftingrankRequired = {GetItemLinkRequiredCraftingSkillRank,	1, 'value', 	1}
itemFunctions.crownStore 	= {IsItemFromCrownStore, 						1, 'return', 	2, ruleEnv.GET_BOOL } -- integrated in itemLink allFlags
itemFunctions.crownCrate 	= {IsItemFromCrownCrate, 						1, 'return', 	2, ruleEnv.GET_BOOL } -- integrated in itemLink allFlags
itemFunctions.crownGems 	= {GetNumCrownGemsFromItemManualGemification, 	1, 'return', 	2} -- needed? -- NOT interleave save
itemFunctions.equipType 	= {GetItemLinkEquipType, 						1, 'table', 	1}
itemFunctions.id 			= {GetItemLinkItemId, 							1, 'value', 	1}
itemFunctions.instanceId 	= {GetItemInstanceId, 							1, 'value', 	2} -- not needed -- NOT interleave save
itemFunctions.quality 		= {GetItemLinkQuality, 							1, 'table', 	1}
itemFunctions.traitInfo 	= {GetItemTraitInformationFromItemLink,			1, 'table', 	1} -- not needed, guess not that important -- NOT interleave save
itemFunctions.trait 		= {GetItemLinkTraitType, 						1, 'table', 	1}
itemFunctions.traitCategory = {GetItemLinkTraitCategory, 				    1, 'table', 	1}
itemFunctions.style 		= {RbI.GetItemLinkItemStyleFix, 				1, 'table', 	1}
itemFunctions.type 			= {GetItemLinkItemType, 						1, 'table', 	1}
itemFunctions.sType 		= {GetItemLinkItemType, 						2, 'table', 	1} -- 'SpecializedItemType'
itemFunctions.weaponType 	= {GetItemLinkWeaponType, 						1, 'table', 	1}
itemFunctions.glyphMinLevel = {GetItemLinkGlyphMinLevels, 					1, 'value', 	1}
itemFunctions.glyphMinCP 	= {GetItemLinkGlyphMinLevels, 					2, 'value', 	1}
itemFunctions.name 			= {GetItemLinkName, 							1, 'value', 	1, ruleEnv.GET_STRING }
itemFunctions.set 			= {GetItemLinkSetInfo, 							1, 'return', 	1, ruleEnv.GET_BOOL }
itemFunctions.setBonusCount	= {GetItemLinkSetInfo, 							3, 'return', 	1}
itemFunctions.setId 		= {GetItemLinkSetInfo, 							6, 'value', 	1}
itemFunctions.setName 		= {GetItemLinkSetInfoSetName,					1, 'value', 	1, ruleEnv.GET_STRING }
itemFunctions.value 		= {GetItemLinkValue, 							1, 'return', 	1}
itemFunctions.junked 		= {IsItemJunk, 									1, 'return', 	2} -- not needed, junked  executes stackwise, stacking on junk is o.k. -- NOT interleave save
itemFunctions.crafted 		= {IsItemLinkCrafted, 							1, 'return', 	1, ruleEnv.GET_BOOL }
itemFunctions.creator 		= {GetItemCreatorName, 							1, 'value', 	2, ruleEnv.GET_STRING } -- not needed, guess not that important -- NOT interleave save
itemFunctions.recipeKnown 	= {IsItemLinkRecipeKnown, 						1, 'return', 	1, ruleEnv.GET_BOOL }
itemFunctions.motifKnown 	= {IsItemLinkBookKnown, 						1, 'return', 	1, ruleEnv.GET_BOOL }
itemFunctions.stackable 	= {IsItemLinkStackable, 						1, 'return', 	1, ruleEnv.GET_BOOL }
itemFunctions.stolen 		= {IsItemLinkStolen, 							1, 'return', 	1, ruleEnv.GET_BOOL }
itemFunctions.unique 		= {IsItemLinkUnique, 							1, 'return', 	1, ruleEnv.GET_BOOL }
itemFunctions.setcollection	= {IsItemLinkSetCollectionPiece, 				1, 'return', 	1, ruleEnv.GET_BOOL }
itemFunctions.reconstructed	= {IsItemLinkReconstructed, 					1, 'return', 	1, ruleEnv.GET_BOOL }
itemFunctions.sellinformation = {GetItemSellInformation,					1, 'return', 	2} -- NOT interleave save
itemFunctions.gearCraftingType   = {GetItemLinkCraftingSkillType,		   	1, 'table', 	1}
itemFunctions.recipeCraftingType = {GetItemLinkRecipeCraftingSkillType,   	1, 'table', 	1}
itemFunctions.filterType    = {GetItemLinkFilterTypeInfo,   				1, 'matrix', 	1}

for name, itemFunction in pairs(itemFunctions) do

	local ItemDataFunction = itemFunction[1]
	local itemDataIndex = itemFunction[2]
	local dataRetrievalType = itemFunction[3]
	local inputType = itemFunction[4]
	local returnType = itemFunction[5]

	if(dataRetrievalType == 'table') then
		ruleEnv.Register(name, function(...) return CheckItemDataByTable(ItemDataFunction, itemDataIndex, name, inputType, ...) end, ruleEnv.MATCH_FILTER)
		ruleEnv.Register(name..'GetValue', function() return ReturnItemDataValue(ItemDataFunction, itemDataIndex, inputType) end)
	elseif(dataRetrievalType == 'value') then
		ruleEnv.Register(name, function(...) return CheckItemDataByValue(returnType, ItemDataFunction, itemDataIndex, inputType, ...) end, ruleEnv.MATCH_FILTER)
	elseif(dataRetrievalType == 'matrix') then
		ruleEnv.Register(name, function(...) return CheckItemDataByMatrix(ItemDataFunction, name, inputType, ...) end, ruleEnv.MATCH_MATRIX)
		ruleEnv.Register(name..'GetValue', function() return ReturnItemDataValueAll(ItemDataFunction, inputType) end)
	else
		ruleEnv.Register(name, function() return ReturnItemDataValue(ItemDataFunction, itemDataIndex, inputType) end, returnType)
	end
end

-- proposed by user Random
ruleEnv.Register("quickslotted", function()
	local bag = ctx.bag
	local slot = ctx.slot
	local itemLink = ctx.itemLink

	if(itemLink ~= nil) then
		local lookup = RbI.GetLookup(bag)
		if(lookup ~= nil) then
			-- GetSlotDatas' returned table must not be written to!
			-- clear! from linked table an value is returned and saved, no link remains
			local itemData = {next(lookup.GetSlotDatas(itemLink))}
			itemData = itemData[2] or {} -- get table of items in bag fitting the itemLink
			itemData = itemData[1] or {} -- get the first item in table
			return itemData.quickslotted
		end
	else
		d("lookup2")
		d(tostring(GetItemCurrentActionBarSlot(bag, slot) ~= nil))
	end
	return GetItemCurrentActionBarSlot(bag, slot) ~= nil
end, ruleEnv.GET_BOOL)

ruleEnv.Register("price", function(itemLink)
	local itemLink = ctx.itemLink or itemLink
	local price
	if(LibPrice ~= nil) then
		price = LibPrice.ItemLinkToPriceGold(itemLink)
	end
	return price or GetItemLinkValue(itemLink)
end)

local function checkFCOIS()
	local fcois = FCOIS
	if(fcois == nil) then
		if(RbI.account.settings.fcois) then -- no fcois, but we need it
			ruleEnv.error('Required Addon FCOIS was not found')
		end
	else
		if(RbI.FcoisControllerUICheck() == false) then -- fcois, but controller is bad
			ruleEnv.error('FCOIS does not work properly with ControllerUI, as found')
		end
	end
	return fcois
end

-- possibly interesting for lookup
ruleEnv.Register("locked", function(bag, slot)
	local bag = ctx.bag or bag
	local slot = ctx.slot or slot
	local result = IsItemPlayerLocked(bag, slot)
	local fcois = checkFCOIS()
	if(fcois ~= nil and result == false) then
		local FCOISettings = fcois.BuildAndGetSettingsFromExternal()
		if (FCOISettings ~= nil) then
			result = fcois.IsMarked(bag, slot, FCOIS_CON_ICON_LOCK)
		end
	end
	return result
end, ruleEnv.GET_BOOL)

ruleEnv.Register("usable", function()
	local usable, onlyFromActionSlot = IsItemUsable(ctx.bag, ctx.slot)
	return usable and not onlyFromActionSlot
end, ruleEnv.GET_BOOL)

ruleEnv.Register("canRefine", function()
	local bag = ctx.bag
	local slot = ctx.slot
	local station = ctx.station
	local result = false
	if(station == nil) then
		result = CanItemBeRefined(bag, slot, CRAFTING_TYPE_BLACKSMITHING)
				or CanItemBeRefined(bag, slot, CRAFTING_TYPE_CLOTHIER)
				or CanItemBeRefined(bag, slot, CRAFTING_TYPE_JEWELRYCRAFTING)
				or CanItemBeRefined(bag, slot, CRAFTING_TYPE_WOODWORKING)
	else
		result = CanItemBeRefined(bag, slot, station)
	end
	return result
end, ruleEnv.GET_BOOL)

ruleEnv.Register("canDeconstruct", function()
	local bag = ctx.bag
	local slot = ctx.slot
	local station = ctx.station
	local result = false
	if(station == nil) then
		result = CanItemBeDeconstructed(bag, slot, CRAFTING_TYPE_BLACKSMITHING)
				or CanItemBeDeconstructed(bag, slot, CRAFTING_TYPE_CLOTHIER)
				or CanItemBeDeconstructed(bag, slot, CRAFTING_TYPE_JEWELRYCRAFTING)
				or CanItemBeDeconstructed(bag, slot, CRAFTING_TYPE_WOODWORKING)
	else
		result = CanItemBeDeconstructed(bag, slot, station)
	end
	return result
end, ruleEnv.GET_BOOL)

ruleEnv.Register("canExtract", function()
	local bag = ctx.bag
	local slot = ctx.slot
	return CanItemBeDeconstructed(bag, slot, CRAFTING_TYPE_ENCHANTING)
end, ruleEnv.GET_BOOL)

local validComparators = {['>']=true, ['>=']=true, ['==']=true, ['<=']=true, ['<']=true, ['~=']=true}
local function CountBag(bag, comparator, value)

	local itemLink = ctx.itemLink
	local bag = bag


	-- if comparator is left blank the mere count of items is returned instead of an evaluation being done
	if(comparator == '' or comparator == nil) then
		if(bag == BAG_VIRTUAL) then
			bag = 3
		end
		return ({GetItemLinkStacks(itemLink)})[bag]
	end

	if(not validComparators[tostring(comparator)]) then
		ruleEnv.errorInvalidArgument(1, comparator, validComparators, "Comparator")
	end

	-- comparator was recognized, begin evaluation
	local itemData = ctx.amountHelper.count[bag]

	ctx.amountHelper[bag] = ctx.amountHelper[bag] or {}
	ctx.amountHelper[bag][value-1] = true
	ctx.amountHelper[bag][value]   = true
	ctx.amountHelper[bag][value+1] = true

	-- enter value for calculation of amount only if we are not in calculations already
	if(itemData == nil) then
		local lookup = RbI.GetLookup(bag)
		if(lookup ~= nil) then
			-- GetSlotDatas' returned table must not be written to!
			-- clear! from linked table an value is returned and saved, no link remains
			itemData = {next(lookup.GetSlotDatas(itemLink))}
			itemData = itemData[2] or {}
			itemData = itemData.count or {}
			itemData = itemData[bag]
			--RbI.ClearLookup(bag) -- not needes because lookup is not "create = true"
		end
	end

	if(bag == BAG_VIRTUAL) then
		bag = 3
	end

	-- fallback to zos function if no other data was found
	itemData = itemData or ({GetItemLinkStacks(itemLink)})[bag]
	return (comparator == '>'  and itemData >  value)
		or (comparator == '>=' and itemData >= value)
		or (comparator == '==' and itemData == value)
		or (comparator == '<=' and itemData <= value)
		or (comparator == '<'  and itemData <  value)
		or (comparator == '~=' and itemData ~= value)
end

ruleEnv.Register("countBackpack", function(comparator, value)
	return CountBag(BAG_BACKPACK, comparator, value)
end)
ruleEnv.Register("countBank", function(comparator, value)
	return CountBag(BAG_BANK, comparator, value)
end)
ruleEnv.Register("countCraftbag", function(comparator, value)
	return CountBag(BAG_VIRTUAL, comparator, value)
end)

ruleEnv.Register("setItemCollected", function()
	local itemLink = ctx.itemLink
    if (not IsItemLinkSetCollectionPiece(itemLink)) then
		return false
	end
    local setId = select(6, GetItemLinkSetInfo(itemLink, false))
    local collectionSlot = GetItemLinkItemSetCollectionSlot(itemLink)
    return IsItemSetCollectionSlotUnlocked(setId, collectionSlot)
end, ruleEnv.GET_BOOL)

local setTypes = {}
ruleEnv.Register("setType", function(...)
	local input = {...}
	if(#input == 0) then
		ruleEnv.error('No input was found')
	end
	if(LibSets == nil) then
		ruleEnv.error('Required Library LibSets was not found')
	end
	if(LibSets.checkIfSetsAreLoadedProperly() == false) then
		--LibSets is currentls scanning and/or not ready!
		ruleEnv.error('Required Library LibSets was not found ready')
	end

	if(not setTypes.rbiInitialized) then
		for setType in pairs(LibSets.GetAllSetTypes()) do
			setTypes[string.lower(LibSets.GetSetTypeName(setType))] = setType
		end
		setTypes['none'] = 0
		setTypes.rbiInitialized = true
	end

	local itemLink = ctx.itemLink
	local isSet, _, setId = LibSets.IsSetByItemLink(itemLink)
	if(isSet) then
		for i = 1, #input do
			local inputI = string.lower(input[i])
			if (setTypes[inputI] == nil) then
				ruleEnv.errorInvalidArgument(i, inputI, setTypes)
			elseif(setTypes[inputI] == LibSets.GetSetType(setId)) then
				return true
			end
		end
	end
	return false
end, ruleEnv.MATCH_FILTER)

ruleEnv.Register("vouchers", function()
	local itemLink = ctx.itemLink
	local itemType, itemSubType = GetItemLinkItemType(itemLink)
	local vouchers = 0
	if(itemType == ITEMTYPE_MASTER_WRIT or itemSubType == SPECIALIZED_ITEMTYPE_MASTER_WRIT) then
		vouchers = tonumber(string.match(GenerateMasterWritRewardText(itemLink), "[%d]+"))
	end
	return vouchers
end)

local soulGemTypeGet = ruleEnv.Register("soulGemTypeGetValue", function()
	local bag = ctx.bag
	local slot = ctx.slot
	local itemLink = ctx.itemLink
	local itemType, itemSubType = GetItemLinkItemType(itemLink)
	if(itemType == ITEMTYPE_SOUL_GEM or itemSubType == SPECIALIZED_ITEMTYPE_SOUL_GEM) then
		local soulGemType
		if(GetItemLinkQuality(itemLink) == ITEM_QUALITY_ARCANE) then
			soulGemType = SOUL_GEM_TYPE_FILLED -- crown soulgems are always full but SoulGemType is "empty"
		else
			soulGemType = select(2, GetSoulGemItemInfo(bag, slot))
		end
		return soulGemType
	end
end)

ruleEnv.Register("soulGemType", function(...)
	local soulGemType = soulGemTypeGet()
	if(soulGemType == nil) then return false end
	local args = ruleEnv.GetArguments('soulGemType', true, 'soulGemType', ...)
	return utils.TableContainsValue(args, soulGemType)
end, ruleEnv.MATCH_FILTER)

ruleEnv.Register("autoCategory", function(...)
	local input = {...}
	if(#input == 0) then
		ruleEnv.error('No input was found')
	end
	if(AutoCategory == nil) then
		ruleEnv.error('Required Addon AutoCategory was not found')
	end
	local bag = ctx.bag
	local slot = ctx.slot
	local _, category = AutoCategory:MatchCategoryRules(bag, slot)
	category = string.lower(category)
	return utils.IsIncludedInList({category}, input)
end, ruleEnv.MATCH_FILTER)

ruleEnv.Register("tag", function(...)
	local input = {...}
	if(#input == 0) then
		ruleEnv.error('No input was found')
	end
	local itemLink = ctx.itemLink
	local tags = {}
	for i = 0, GetItemLinkNumItemTags(itemLink) do
		local tag = GetItemLinkItemTagInfo(itemLink, i)
		if(tag ~= nil and tag ~= '') then
			table.insert(tags, zo_strformat("<<1>>", string.lower(tag)))
		end
	end
	return textCheck(input, tags, false)
end, ruleEnv.MATCH_FILTER)

ruleEnv.Register("fcoisPermission", function(taskName)
	local fcois = checkFCOIS()
	if(fcois == nil) then return true end

	local result = true
	local bag = ctx.bag
	local slot = ctx.slot
	local taskName = taskName or ctx.taskName
	if(taskName == 'Junk') then
		result = not fcois.IsJunkLocked(bag, slot)
	elseif(taskName == 'Launder') then
		result = not fcois.IsLaunderLocked(bag, slot)
	elseif(taskName == 'Fence') then
		result = not fcois.IsFenceSellLocked(bag, slot)
	elseif(taskName == 'Sell') then
		result = not fcois.IsVendorSellLocked(bag, slot)
	elseif(taskName == 'Destroy') then
		result = not fcois.IsDestroyLocked(bag, slot)
	elseif(taskName == 'Deconstruct') then
		result = not fcois.IsDeconstructionLocked(bag, slot)
	elseif(taskName == 'Extract') then
		result = not fcois.IsEnchantingExtractionLocked(bag, slot)
	elseif(taskName == 'Refine') then
		result = not fcois.IsRefinementLocked(bag, slot)
	end
	return result
end)

ruleEnv.Register("fcoisMarker", function(...)
	local fcois = checkFCOIS()
	if(fcois == nil) then return false end

	local noInput = select('#', ...) == 0
	local input = ruleEnv.GetArgumentsMap("FcoisMarker", false, "fcoisMark", ...)
	local bag = ctx.bag
	local slot = ctx.slot
	local isMarked, marker = fcois.IsMarked(bag, slot, -1)
	if(isMarked and marker ~= nil) then
		for i, mark in ipairs(marker) do
			if(mark == true) then
				if(noInput) then return true end
				if(input[i]) then
					return true
				end
			end
		end
	end
	return false
end)

-- @require CraftStore
ruleEnv.Register("needTraitInOrder", function(...)
	local charNames = ruleEnv.GetCharactersNames(true, ...)
	return ext.RequireMultiCharactersApi().CanResearch(true, charNames)
end, ruleEnv.MATCH_FILTER)
-- @require CraftStore
ruleEnv.Register("needTrait", function(...)
	local charNames = ruleEnv.GetCharactersNames(false, ...)
	return ext.RequireMultiCharactersApi().CanResearch(false, charNames)
end, ruleEnv.GET_BOOL)

-- @require CraftStore
ruleEnv.Register("needLearnInOrder", function(...)
	local charNames = ruleEnv.GetCharactersNames(true, ...)
	return ext.RequireMultiCharactersApi().CanLearn(true, charNames)
end, ruleEnv.MATCH_FILTER)
-- @require CraftStore
ruleEnv.Register("needLearn", function(...)
	local charNames = ruleEnv.GetCharactersNames(false, ...)
	return ext.RequireMultiCharactersApi().CanLearn(false, charNames)
end, ruleEnv.GET_BOOL)

ruleEnv.Register("costPerVoucher", function()
	if(WritWorthy == nil) then ruleEnv.errorAddonRequired('WritWorthy') end
	local itemLink = ctx.itemLink
	local vouchers = self.vouchers(itemLink)
	local cost = -1
	if(vouchers > 0) then
		local matCost = WritWorthy.ToMatCost(itemLink)
		if(matCost ~= nil) then
			cost = matCost / vouchers
		end
	end
	return cost
end)

ruleEnv.Register("canCraftMasterwrit", function()
	if(WritWorthy == nil) then ruleEnv.errorAddonRequired('WritWorthy') end
	if(not self.type('masterwrit')) then return false end
	local mat_list, know_list, parser = WritWorthy.ToMatKnowList(ctx.itemLink)
	if(know_list) then
		for _, know in ipairs(know_list) do
			if(not know.is_known) then return false end
		end
	end
	local mats = WritWorthy.MatHaveCtTooltipText(mat_list)
	if(mats ~= nil and mats ~='') then return false end
	return true
end, ruleEnv.GET_BOOL)

ruleEnv.Register("level", function()
	local itemLink = ctx.itemLink
	-- for material take max gear cp/lvl which can be crafted
	local level = RbI.itemCatalog.MaterialRequirementLevel[GetItemLinkItemId(itemLink)]
	if(level ~= nil) then
		level = level[2]
		if(level > GetMaxLevel()) then
			level = GetMaxLevel()
		end
	else
		-- if level returned is nil (itemLink was no material)
		level = GetItemLinkRequiredLevel(itemLink)
	end
	return level
end)

ruleEnv.Register("cp", function()
	local itemLink = ctx.itemLink
	-- for material take max gear cp/lvl which can be crafted
	local cp = RbI.itemCatalog.MaterialRequirementLevel[GetItemLinkItemId(itemLink)]
	if(cp ~= nil) then
		cp = cp[2]
		if(cp > GetMaxLevel()) then
			-- recalculate returned cp from scaled version
			cp = cp - GetMaxLevel()
		else
			cp = 0
		end
	else
		-- if cp returned is nil (itemLink was no material)
		cp = GetItemLinkRequiredChampionPoints(itemLink)
	end
	return cp
end)

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Crafting skill functions --------------------------------------------------
------------------------------------------------------------------------------

-- TODO: also furnishings!? GetItemLinkRecipeResultItemLink,  GetNumRecipeLists, GetRecipeInfo, GetRecipeResultItemLink
-- @param 'craftingSkillName': = nil: determines the crafting type from the item, otherwise the given craftingSkillName will be used
-- @return the number of the skill [0-7], 0: none, 7:jewelry
local CraftingTypeGet = ruleEnv.Register("craftingTypeGetValue", function(craftingSkillName)
	local craftingSkill = nil
	if(craftingSkillName == nil) then -- no argument => auto retrieval craftskill
		craftingSkill = GetItemLinkCraftingSkillType(ctx.itemLink) -- gearCraftingType-method
		if(craftingSkill == CRAFTING_TYPE_INVALID) then
			craftingSkill = GetItemLinkRecipeCraftingSkillType(ctx.itemLink) -- recipeCraftingType-method
		end
		if(craftingSkill == CRAFTING_TYPE_INVALID) then
			if(self.type('material_raw_jewelrytrait')) then
				craftingSkill = CRAFTING_TYPE_JEWELRYCRAFTING
			else
				local itemLinkId = GetItemLinkItemId(ctx.itemLink)
				craftingSkill = RbI.itemCatalog.furnishingCraftingType[itemLinkId] or 0
			end
		end
		return craftingSkill
	else
		craftingSkillName = string.lower(craftingSkillName)
	end
	craftingSkill = RbI.dataType.gearCraftingType[craftingSkillName] -- same mapping as recipeCraftingType
	if(craftingSkill == nil) then
		ruleEnv.errorInvalidArgument2(1, craftingSkillName, 'gearCraftingType', 'craftingType')
	end
	return craftingSkill
end, ruleEnv.GET_INT)

ruleEnv.Register("craftingType", function(...)
	local craftingSkill = CraftingTypeGet()
	if(#{...} == 0) then
		return craftingSkill
	end
	local args = ruleEnv.GetArguments('CraftingType', true, 'gearCraftingType', ...)
	return utils.TableContainsValue(args, craftingSkill)
end, ruleEnv.MATCH_FILTER)

local craftingLevel = ruleEnv.Register("craftingLevel", function(craftingSkillName)
	local craftingSkill = CraftingTypeGet(craftingSkillName)
	if(craftingSkill == 0) then return RbI.NaN end
	local skillType, skillId = GetCraftingSkillLineIndices(craftingSkill)
	local _, level = GetSkillLineInfo(skillType, skillId)
	return level or 0
end, ruleEnv.GET_INT)
ruleEnv.Register("skilllinelevel", craftingLevel) -- deprecated 'skilllinelevel'

-- @return the minimum level of all given characters
-- @require CraftStore
ruleEnv.Register("craftingLevelMin", function(...)
	local craftingSkill = CraftingTypeGet()
	if(craftingSkill == 0) then return RbI.NaN end
	local charNames = ruleEnv.GetCharactersNames(true, ...)
	return ext.RequireMultiCharactersApi().GetCraftingLevelMin(craftingSkill, 50, charNames)
end, ruleEnv.GET_INT)

-- @return the maximum level of all given characters
-- @require CraftStore
ruleEnv.Register("craftingLevelMax", function(...)
	local craftingSkill = CraftingTypeGet()
	if(craftingSkill == 0) then return RbI.NaN end
	local charNames = ruleEnv.GetCharactersNames(true, ...)
	return ext.RequireMultiCharactersApi().GetCraftingLevelMax(craftingSkill, 0, charNames)
end, ruleEnv.GET_INT)

local bonusTable = {
	[CRAFTING_TYPE_ALCHEMY] = NON_COMBAT_BONUS_ALCHEMY_LEVEL,
	[CRAFTING_TYPE_BLACKSMITHING] = NON_COMBAT_BONUS_BLACKSMITHING_LEVEL,
	[CRAFTING_TYPE_CLOTHIER] = NON_COMBAT_BONUS_CLOTHIER_LEVEL,
	[CRAFTING_TYPE_ENCHANTING] = NON_COMBAT_BONUS_ENCHANTING_LEVEL,
	[CRAFTING_TYPE_PROVISIONING] = NON_COMBAT_BONUS_PROVISIONING_LEVEL,
	[CRAFTING_TYPE_WOODWORKING] = NON_COMBAT_BONUS_WOODWORKING_LEVEL,
	[CRAFTING_TYPE_JEWELRYCRAFTING] = NON_COMBAT_BONUS_JEWELRYCRAFTING_LEVEL,
}
ruleEnv.Register("craftingRank", function()
	local craftingSkill = CraftingTypeGet()
	if(craftingSkill == 0) then return RbI.NaN end
	local bonus = bonusTable[craftingSkill]
	return GetNonCombatBonus(bonus) or 0
end, ruleEnv.GET_INT)

-- @return the minimum rank of all given characters
-- @require CraftStore
ruleEnv.Register("craftingRankMin", function(...)
	local craftingSkill = CraftingTypeGet()
	if(craftingSkill == 0) then return RbI.NaN end
	local charNames = ruleEnv.GetCharactersNames(true, ...)
	return ext.RequireMultiCharactersApi().GetCraftingRankMin(craftingSkill, 50, charNames)
end, ruleEnv.GET_INT)

-- @return the maximum rank of all given characters
-- @require CraftStore
ruleEnv.Register("craftingRankMax", function(...)
	local craftingSkill = CraftingTypeGet()
	if(craftingSkill == 0) then return RbI.NaN end
	local charNames = ruleEnv.GetCharactersNames(true, ...)
	return ext.RequireMultiCharactersApi().GetCraftingRankMax(craftingSkill, 0, charNames)
end, ruleEnv.GET_INT)


local bonusTableExtract = {
	[CRAFTING_TYPE_BLACKSMITHING] = NON_COMBAT_BONUS_BLACKSMITHING_EXTRACT_LEVEL,
	[CRAFTING_TYPE_CLOTHIER] = NON_COMBAT_BONUS_CLOTHIER_EXTRACT_LEVEL,
	[CRAFTING_TYPE_WOODWORKING] = NON_COMBAT_BONUS_WOODWORKING_EXTRACT_LEVEL,
	[CRAFTING_TYPE_JEWELRYCRAFTING] = NON_COMBAT_BONUS_JEWELRYCRAFTING_EXTRACT_LEVEL,
}
-- GetNonCombatBonus returns 0 if the character has not the skillline activated (craftingRank==0)
-- returns 1-4 for level 0-3, so we have to subtract 1 for this
ruleEnv.Register("craftingExtractBonus", function()
	local craftingSkill = CraftingTypeGet()
	if(craftingSkill == 0) then return RbI.NaN end
	local bonus = bonusTableExtract[craftingSkill]
	local result = GetNonCombatBonus(bonus)
	if(result == 0) then
		return result
	end
	return result - 1
end, ruleEnv.GET_INT)

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Champion Point Tree functions ---------------------------------------------
------------------------------------------------------------------------------

-- GetChampionSkillCurrentBonusText()
-- GetChampionSkillData()
-- GetChampionSkillDescription()
-- GetChampionSkillId()
-- GetChampionSkillJumpPoints()
-- GetChampionSkillLinkIds()
-- GetChampionSkillMaxPoints()
-- GetChampionSkillName()
-- GetChampionSkillPosition()
-- GetChampionSkillType()

local function IsCPSlotted(skillId)
	for index=1, 4 do
		if GetSlotBoundId(index, HOTBAR_CATEGORY_CHAMPION) == skillId then
			return true
		end
	end
	return false
end

-- required points for first jump point
local function RequiredPoints(cSkillId)
	if not DoesChampionSkillHaveJumpPoints(cSkillId) then return 1 end
	local firstJumpPoint
	_, firstJumpPoint = GetChampionSkillJumpPoints(cSkillId)
	return firstJumpPoint
end

local function IsCPSlotable(skillId)
	return CanChampionSkillTypeBeSlotted(GetChampionSkillType(skillId))
end

-- return <int> if its a passive or slotted it will return the spent points into this skill
-- @NotItemRelated
ruleEnv.Register("cSkill", function(skillName)
	skillName = string.lower(skillName)
	if(not IsChampionSystemUnlocked()) then
		return false
	end
	local cSkillId = ruleEnv.GetArguments("cSkillName", true, "cSkill", skillName)[1]
	if(not IsCPSlotable(cSkillId) or IsCPSlotted(cSkillId)) then
		return GetNumPointsSpentOnChampionSkill(cSkillId)
	end
	return 0
end, ruleEnv.NI_INT)

-- return <int> how many champion points spended in this skill
-- @NotItemRelated
ruleEnv.Register("cSkillSpent", function(skillName)
	skillName = string.lower(skillName)
	if(not IsChampionSystemUnlocked()) then
		return 0
	end
	local cSkillId = ruleEnv.GetArguments("cSkillName", true, "cSkill", skillName)[1]
	return GetNumPointsSpentOnChampionSkill(cSkillId)
end, ruleEnv.NI_INT)

-- return <boolean>
-- @NotItemRelated
ruleEnv.Register("cSkillSlotted", function(skillName)
	skillName = string.lower(skillName)
	if(not IsChampionSystemUnlocked()) then
		return false
	end
	local cSkillId = ruleEnv.GetArguments("cSkillName", true, "cSkill", skillName)[1]
	return IsCPSlotted(cSkillId)
end, ruleEnv.NI_BOOL)

-- return <boolean> true, when it is a slotable star and there are enough points
-- @NotItemRelated
ruleEnv.Register("cSkillSlotable", function(skillName)
	skillName = string.lower(skillName)
	if(not IsChampionSystemUnlocked()) then
		return false
	end
	local cSkillId = ruleEnv.GetArguments("cSkillName", true, "cSkill", skillName)[1]
	return IsCPSlotable(cSkillId) and GetNumPointsSpentOnChampionSkill(cSkillId) >= RequiredPoints(cSkillId)
end, ruleEnv.NI_BOOL)

-- @NotItemRelated
ruleEnv.Register("cSkillTypeSlotable", function(skillName)
	skillName = string.lower(skillName)
	if(not IsChampionSystemUnlocked()) then
		return false
	end
	local cSkillId = ruleEnv.GetArguments("cSkillName", true, "cSkill", skillName)[1]
	return IsCPSlotable(cSkillId)
end, ruleEnv.NI_BOOL)

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- @NotItemRelated -----------------------------------------------------------
------------------------------------------------------------------------------


-- @NotItemRelated
ruleEnv.Register("freeslots", function(...)

	local input = {...}
	if(#input == 0) then
		ruleEnv.error('No input was found')
	end

	local count = 0
	local data = RbI.dataType['bag']

	for i = 1, #input do
		if (data[input[i]] == nil) then
			ruleEnv.errorInvalidArgument(i, input[i], data)
		else
			count = count + GetNumBagFreeSlots(data[input[i]])
		end
	end
	return count
end, ruleEnv.NI_INT)

-- implemented by MegwynW
-- @NotItemRelated
ruleEnv.Register("esoplus", function()
	return IsESOPlusSubscriber()
end, ruleEnv.NI_BOOL)

-- implemented by MegwynW
-- @NotItemRelated
ruleEnv.Register("ischaracter", function(...)
	local charNames = ruleEnv.GetCharactersNames(nil, ...)
	local thisCharactersName = GetUnitName('player')

	for i = 1, #charNames do
		if (thisCharactersName == charNames[i]) then
			return true
		end
	end

	return false
end, ruleEnv.NI_BOOL)

----------------------------------------
-- simplification functions ------------
----------------------------------------

ruleEnv.Register("material_alchemy", function()
	return self.type('reagent'
							 ,'potion_base'
							 ,'poison_base')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("item_enchanting", function()
	return self.type('glyph_armor'
							 ,'glyph_weapon'
							 ,'glyph_jewelry')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("glyph", function(...)
	return self.type('glyph_armor'
							,'glyph_weapon'
							,'glyph_jewelry')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("item_jewelry", function()
	return self.equiptype('ring'
								   ,'neck')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("armor_clothier", function()
	return self.armortype('light'
								  ,'medium')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("item_clothier", function()
	return self.armor_clothier()
end, ruleEnv.GET_BOOL)

ruleEnv.Register("weapon_woodworking", function()
	return self.weapontype('bow'
								   ,'firestaff'
								   ,'froststaff'
								   ,'healingstaff'
								   ,'lightningstaff'
								   ,'shield')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("item_woodworking", function()
	return self.weapon_woodworking()
end, ruleEnv.GET_BOOL)

ruleEnv.Register("armor_blacksmithing", function()
	return self.armortype('heavy')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("weapon_blacksmithing", function()
	return self.weapontype('axe'
								   ,'dagger'
								   ,'mace'
								   ,'sword'
								   ,'battleaxe'
								   ,'maul'
								   ,'greatsword')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("item_blacksmithing", function()
	return self.weapon_blacksmithing()
		or self.armor_blacksmithing()
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material_jewelrytrait", function()
	return self.type('material_raw_jewelrytrait'
							 ,'material_refined_jewelrytrait')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material_jewelry", function()
	return self.type('material_raw_jewelry'
							 ,'material_refined_jewelry'
							 ,'booster_refined_jewelry')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material_woodworking", function()
	return self.type('material_raw_woodworking'
							 ,'material_refined_woodworking'
							 ,'booster_woodworking')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material_clothier", function()
	return self.type('material_raw_clothier'
							 ,'material_refined_clothier'
							 ,'booster_clothier')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material_blacksmithing", function()
	return self.type('material_raw_blacksmithing'
							 ,'material_refined_blacksmithing'
							 ,'booster_blacksmithing')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material_style", function()
	return self.type('material_refined_style'
							 ,'material_raw_style')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material_enchantment", function()
	return self.type('aspect'
							 ,'essence'
							 ,'potency')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material_refined_trait", function()
	return self.type('material_armortrait'
							 ,'material_weapontrait'
							 ,'material_refined_jewelrytrait')
end, ruleEnv.GET_BOOL
)

ruleEnv.Register("material_raw_trait", function()
	return self.type('material_raw_jewelrytrait')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material_trait", function()
	return self.material_refined_trait()
		or self.material_raw_trait()
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material_refined_booster", function()
	return self.type('booster_blacksmithing'
							 ,'booster_clothier'
							 ,'booster_woodworking'
							 ,'booster_refined_jewelry')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material_raw_booster", function()
	return self.type('booster_raw_jewelry')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material_booster", function()
	return self.material_refined_booster()
		or self.material_raw_booster()
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material_refined", function()
	return self.type('material_refined_blacksmithing'
							 ,'material_refined_clothier'
							 ,'material_refined_woodworking'
							 ,'material_refined_jewelry'
							 ,'material_refined_style'
							 ,'material_furnishing'
							 ,'ingredient')
		or self.material_refined_trait()
		or self.material_refined_booster()
		or self.material_alchemy()
		or self.material_enchantment()
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material_raw", function()
	return self.type('material_raw_blacksmithing'
							 ,'material_raw_clothier'
							 ,'material_raw_woodworking'
							 ,'material_raw_jewelry'
							 ,'material_raw_jewelrytrait'
							 ,'material_raw_style'
							 ,'booster_raw_jewelry')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("material", function()
	return self.material_raw()
		or self.material_refined()
end, ruleEnv.GET_BOOL)

ruleEnv.Register("traited", function()
	return not self.traitcategory('none')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("gear", function()
	return not self.equiptype('none', 'poison', 'costume')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("companion", function()
	return self.filtertype('companion')
end, ruleEnv.GET_BOOL)

ruleEnv.Register("fragment", function()
	return self.stype("key_fragment", "recipe_fragment", "runebox_fragment", "collectible_fragment", "upgrade_fragment")
end, ruleEnv.GET_BOOL)

local function findRule(ruleName)
	local result = RbI.rules.user[ruleName]
	if(result) then return result end
	-- lowercase searching
	for name, fn in pairs(RbI.rules.user) do
		if(name:lower() == ruleName) then
			if(result == nil) then
				result = fn
			else -- found a second rule with that name
				ruleEnv.error("Because of the internal lowercase rule references, we found at least two rules with the same name: '"..result.GetName().."' and '"..name.."'. Please rename them.")
			end
		end
	end
	return result
end
local rule = ruleEnv.Register("rule", function(ruleName)
	local rule = findRule(string.lower(ruleName))
	if(rule == nil) then
		ruleEnv.errorInvalidArgument(nil, ruleName, RbI.rules.user, 'Rulename')
	end
	return ruleEnv.EvaluateRule(rule)
end)
ruleEnv.Register("_", rule)

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Test and alpha functions --------------------------------------------------
------------------------------------------------------------------------------

ruleEnv.Register("dump", function(...)
	d(ctx.itemLink..': '..tostring(...))
	return false
end)

ruleEnv.Register("dumpWhen", function(value, ...)
	if(value) then
		if(select(1, ...) == nil) then
			d(ctx.itemLink..': '..tostring(value))
		else
			d(ctx.itemLink..': '..tostring(...))
		end
	end
	return value
end)

ruleEnv.Register("dumpWhenF", function(value, ...)
	if(value) then
		if(select(1, ...) == nil) then
			d(ctx.itemLink..': '..tostring(value))
		else
			d(ctx.itemLink..': '..tostring(...))
		end
	end
	return false
end)

-- arguments have to strings with rule-content
ruleEnv.Register("ensure", function(...)
	for _, content in pairs({...}) do
		if(type(content) ~= "string") then
			ruleEnv.error("Only string arguments with rulecode are permitted!")
		end
		local result = ruleEnv.RunRule(RbI.BaseRule("ensure", true, content))
		if(not result) then
			ruleEnv.error("Ensure failure "..content.." on item: "..ctx.itemLink)
		end
	end
	return true
end)

ruleEnv.Register("call", function(fn)
	return fn()
end)

-- emits an internal function error
ruleEnv.Register("fail", function(msg)
	ruleEnv.error(msg)
end)

ruleEnv.Register("back", function(...)
	return ...
end)