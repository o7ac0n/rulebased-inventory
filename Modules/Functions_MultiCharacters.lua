-----------------------------------------------------------------------------------
-- Addon Name: Rulebased Inventory
-- Creator: TaxTalis, demawi
-- Addon Ideal: user-defined rule-based actions for inventory application
-- Addon Creation Date: September 11, 2018
--
-- File Name: Ext_CraftApi.lua
-- File Description: Basic character-based knowledge functions. Needs a knowledge
--                   provider like CraftStore addon and a adapter implementation
--                   like Ext_CraftStore.lua
-- Load Order Requirements: early
--
-----------------------------------------------------------------------------------

-- Imports
local RbI = RulebasedInventory
local ext = RbI.extensions
local utils = RbI.utils
local ruleEnv = RbI.ruleEnvironment
local ctx = RbI.executionEnv.ctx
local knowledgeProvider

-- @return array of missing character names
local function checkCraftChars(charNames)
    -- check if all characters are available
    local missingCharacters = {}
    for _, charName in pairs(charNames) do
        if(not knowledgeProvider.IsAvailable(charName)) then
            missingCharacters[#missingCharacters + 1] = charName
        end
    end
    return missingCharacters
end

local craftStoreRequestedCharsCache = {}
local ensureCharactersLoaded = function(charNames)
    local cacheId = utils.ArrayValuesToString(charNames)
    local missingCharacters = craftStoreRequestedCharsCache[cacheId]
    if (missingCharacters == nil) then
        missingCharacters = checkCraftChars(charNames)
        craftStoreRequestedCharsCache[cacheId] = missingCharacters
    end
    if (#missingCharacters > 0) then
        ruleEnv.error('Not all character data is loaded. Please login with the following characters, so that the addon can fill the needed data or exclude them in your rules: ' .. utils.ArrayValuesToString(missingCharacters))
    end
end

-- direct learn check, so we don't need to wait that craft store updates its values after usage
local function canILearn(itemLink)
    local itemType, itemSubType = GetItemLinkItemType(itemLink)
    if(itemType == ITEMTYPE_RECIPE) then
        return not IsItemLinkRecipeKnown(itemLink)
    elseif(itemType == ITEMTYPE_RACIAL_STYLE_MOTIF) then
        return not IsItemLinkBookKnown(itemLink)
    end
    return false
end

local function canIResearch(itemLink)
    return CanItemLinkBeTraitResearched(itemLink)
end

local me = GetUnitName('player')
local function checkLearnResearch(itemLink, inOrder, charNames, meCanLearn, otherCanLearn)
    ensureCharactersLoaded(charNames)
    if(inOrder) then
        for _, charName in pairs(charNames) do
            if(charName == me) then
                return meCanLearn(itemLink)
            elseif(otherCanLearn(itemLink, charName)) then
                return false
            end
        end
        return false
    else
        for _, charName in pairs(charNames) do
            if(charName == me) then
                if(meCanLearn(itemLink)) then
                    return true
                end
            elseif(otherCanLearn(itemLink, charName)) then
                return true
            end
        end
        return false
    end
end

local function CanLearn(inOrder, charNames)
    local itemLink = ctx.itemLink
    local itemType, itemSubType = GetItemLinkItemType(itemLink)
    if(itemType ~= ITEMTYPE_RECIPE and itemType ~= ITEMTYPE_RACIAL_STYLE_MOTIF) then
        return false
    end
    return checkLearnResearch(itemLink, inOrder, charNames, canILearn, knowledgeProvider.IsLearnable)
end

local function CanResearch(inOrder, charNames)
    local itemLink = ctx.itemLink
    return checkLearnResearch(itemLink, inOrder, charNames, canIResearch, knowledgeProvider.IsResearchable)
end

local function GetCraftingLevelMin(craftingSkill, default, charNames)
    ensureCharactersLoaded(charNames)
    local result = default
    for _, charName in pairs(charNames) do
        local level = knowledgeProvider.GetCraftingLevel(charName, craftingSkill) or 0
        result = math.min(result, level)
    end
    return result
end
local function GetCraftingLevelMax(craftingSkill, default, charNames)
    ensureCharactersLoaded(charNames)
    local result = default
    for _, charName in pairs(charNames) do
        local level = knowledgeProvider.GetCraftingLevel(charName, craftingSkill) or 0
        result = math.max(result, level)
    end
    return result
end

local function GetCraftingRankMin(craftingSkill, default, charNames)
    ensureCharactersLoaded(charNames)
    local result = default
    for _, charName in pairs(charNames) do
        local level = knowledgeProvider.GetCraftingRank(charName, craftingSkill) or 0
        result = math.min(result, level)
    end
    return result
end
local function GetCraftingRankMax(craftingSkill, default, charNames)
    ensureCharactersLoaded(charNames)
    local result = default
    for _, charName in pairs(charNames) do
        local level = knowledgeProvider.GetCraftingRank(charName, craftingSkill) or 0
        result = math.max(result, level)
    end
    return result
end

local MultiCharacterApi = {
    CanLearn = CanLearn,
    CanResearch = CanResearch,
    GetCraftingRankMin = GetCraftingRankMin,
    GetCraftingRankMax = GetCraftingRankMax,
    GetCraftingLevelMin = GetCraftingLevelMin,
    GetCraftingLevelMax = GetCraftingLevelMax,
}

function ext.RequireMultiCharactersApi()
    -- currently fixed to craft store, but can be easily any other knowledge provider
    knowledgeProvider = ext.RequireCraftStore()
    return MultiCharacterApi
end


