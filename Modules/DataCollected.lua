-----------------------------------------------------------------------------------
-- Addon Name: Rulebased Inventory
-- Creator: TaxTalis, demawi
-- Addon Ideal: user-defined rule-based actions for inventory application
-- Addon Creation Date: September 11, 2018
--
-- File Name: DataCollected.lua
-- File Description: This file contains data which is needed for rule evaluation
-- Load Order Requirements: after Data, before Item
--
-----------------------------------------------------------------------------------

local RbI = RulebasedInventory

-- get data for functions collected from game variables
local collected = {}
RbI.collectedData = collected

-- imports
local utils = RbI.utils
local dataTypeHandler = RbI.dataTypeHandler

-- dynamic values for names collected from ingame functions
local function CollectByIterator()
    local collector = {
        armorType = {ARMORTYPE_ITERATION_BEGIN, ARMORTYPE_ITERATION_END, "SI_ARMORTYPE"},
        bindType = {BIND_TYPE_ITERATION_BEGIN, BIND_TYPE_ITERATION_END, "SI_BINDTYPE"},
        equipType = {EQUIP_TYPE_ITERATION_BEGIN, EQUIP_TYPE_ITERATION_END, "SI_EQUIPTYPE"},
        quality = {ITEM_QUALITY_ITERATION_BEGIN, ITEM_QUALITY_ITERATION_END, "SI_ITEMQUALITY"},
        traitInfo = {ITEM_TRAIT_INFORMATION_ITERATION_BEGIN, ITEM_TRAIT_INFORMATION_ITERATION_END, "SI_ITEMTRAITINFORMATION"},
        trait = {ITEM_TRAIT_TYPE_ITERATION_BEGIN, ITEM_TRAIT_TYPE_ITERATION_END, "SI_ITEMTRAITTYPE"},
        type = {ITEMTYPE_ITERATION_BEGIN, ITEMTYPE_ITERATION_END, "SI_ITEMTYPE"},
        sType = {SPECIALIZED_ITEMTYPE_ITERATION_BEGIN, SPECIALIZED_ITEMTYPE_ITERATION_END, "SI_SPECIALIZEDITEMTYPE"},
        weaponType = {WEAPONTYPE_ITERATION_BEGIN, WEAPONTYPE_ITERATION_END, "SI_WEAPONTYPE" },
    }

    for enumName, collectorData in pairs(collector) do
        local newData = {}
        for enumIndex = collectorData[1], collectorData[2] do
            local name = GetString(collectorData[3], enumIndex)
            if(name ~= nil and name ~= "" and name:lower() ~= 'do not translate') then
                dataTypeHandler.addData(newData, name, enumIndex)
            end
        end
        collected[enumName] = newData
    end
end
CollectByIterator()

local function CollectSpecials()
    collected.style = {}
    -- collect styles
    for enumIndex = 1, GetNumValidItemStyles() do
        local styleId = GetValidItemStyleId(enumIndex)
        local name = zo_strformat("<<1>>", GetItemStyleName(styleId))
        if(name ~= nil and name ~= "") then
            collected.style[name] = styleId
        end
    end
end
CollectSpecials()

collected.cSkill= {}
local dataType = RbI.dataType
for _, skillId in pairs(dataType.cSkill) do
    local skillName = zo_strformat(SI_CHAMPION_STAR_NAME, GetChampionSkillName(skillId))
    if(type(skillName) == 'string') then -- could be a new value from test-server
        collected.cSkill[skillName] = skillId
    end
end

-- merge collected data into RbI.dataType
for enumName, enum in pairs(collected) do
    local targetEnum = dataType[enumName]
    for key, value in pairs(enum) do
        dataTypeHandler.addData(targetEnum, string.lower(key), value)
    end
end

-- only for test-environment: check data type validity
if(RbI.class.DataDumper ~= nil) then
    for name, data in pairs(RbI.dataType) do
        if(utils.isArray(data)) then
            for _, value in pairs(data) do
                if(value ~= value:lower()) then error('Not lowercased value: '..name..' '..value)  end
            end
        else
            for key, _ in pairs(data) do
                if(type(key) ~= 'string') then
                    error("not a string "..name..' => '..tostring(key))
                end
                if(key ~= key:lower()) then error('Not lowercased key: '..name..' '..key)  end
            end
        end
    end
end