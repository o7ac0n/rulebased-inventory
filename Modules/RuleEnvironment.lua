-----------------------------------------------------------------------------------
-- Addon Name: Rulebased Inventory
-- Creator: TaxTalis, demawi
-- Addon Ideal: user-defined rule-based actions for inventory application
-- Addon Creation Date: September 11, 2018
--
-- File Name: RuleEnvironment.lua
-- File Description: This file manages the environment for rule/function-evaluation
-- 					 it also delivers helper methods for functions
-- Load Order Requirements: before every file that delivers functions or constants
--                          for the rule environment.
--
-----------------------------------------------------------------------------------

-- imports
local RbI = RulebasedInventory
local utils = RbI.utils
local dataTypeHandler = RbI.dataTypeHandler

-- defs
local ruleEnv = {}
RbI.ruleEnvironment = ruleEnv
local ctx = {}
local executionEnv = {
	ctx = ctx
}
executionEnv.self = executionEnv
RbI.executionEnv = executionEnv
local ignoreExecKeys = {}
for key in pairs(executionEnv) do ignoreExecKeys[key] = true end

-- Usage from: Rule+Item
ctx.itemLink = nil
ctx.bag = nil
ctx.slot = nil
ctx.taskName = nil
ctx.station = nil
ctx.task = nil -- object when task starts {}
ctx.item = nil -- object for every item {}
ctx.amountHelper = {} -- Usage from: Rule+Item+Task

ruleEnv.GET_BOOL = "getbool" -- functions without inputs and boolean return type
ruleEnv.GET_INT = "getint" -- functions without inputs and int return type
ruleEnv.GET_STRING = "getstring" -- functions without inputs and string return type
ruleEnv.MATCH_FILTER = "matchfilter" -- functions with key-string-inputs and string return type
ruleEnv.MATCH_MATRIX = "matchmatrix" -- functions with key-string-inputs and multiple string return type
ruleEnv.NI_BOOL = "nibool" -- @NotItemRelated functions with inputs and boolean return type
ruleEnv.NI_INT = "niint" -- @NotItemRelated functions with inputs and int return type

function GetAllCharNames()
	local result = {}
	for i = 1, GetNumCharacters() do
		local charName  = GetCharacterInfo(i)
		charName = zo_strformat(SI_UNIT_NAME, charName)
		result[#result+1] = charName
	end
	return result
end

local me = GetUnitName('player')
local allchars = GetAllCharNames()
executionEnv.defaultchars = allchars -- should be user-editable in the future

local caseSensitiveArgumentedRules = {}
-- TODO: to be implemented
-- all the string-parameters for functions in caseSensitiveArgumentedRules should not be lowered
local function lowerContent(content)
	-- TODO: remove comment lines first
	local loweredContent = string.lower(content)
	local singleQuotedArguments = loweredContent:gmatch("[^\\]'()(.-[^\\])'()")
	local doubleQuotedArguments = loweredContent:gmatch("[^\\]\"()(.-[^\\])\"()")
	local functionCalls = {}
	for startpos, match, endpos in loweredContent:gmatch("()(%w* *)()%(") do
		--print(startpos, "'"..match.."'", endpos-1)
	end

	for functionname in pairs(caseSensitiveArgumentedRules) do

	end
end

-- returns a callable function
function ruleEnv.Compile(content)
	-- code for inheritance
	if(RbI.account.settings.casesensitiverules == false) then
		content = string.lower(content)
	end
	local result = zo_loadstring('return (' .. content .. ')')
	setmetatable(executionEnv, {__index = _G})
	setfenv(result, executionEnv)
	return result
end

local functionStack = {}
function ruleEnv.getStackTrace()
	local stackTrace = {}
	for k,v in pairs(functionStack) do
		stackTrace[k] = v
	end
	return stackTrace
end

local MAX_STACK = 30
local function wrapFunction(name, fn)
	return function(...)
		-- inside ruleEnvironment execution so use specific errors
		table.insert(functionStack, {name, {...}})
		local status, result
		if(#functionStack > MAX_STACK) then
			status, result = pcall(ruleEnv.error, 'Stack overflow error! #'..#functionStack..' (only '..MAX_STACK..' are permitted)')
		else
			status, result = pcall(fn, ...)
		end
		table.remove(functionStack)
		if(not status) then -- we have an error in a function :o
			if(type(result) == "string") then -- unexpected error
				ruleEnv.error(nil, result)
			else
				error(result)
			end
		end
		return result
	end
end

local function GetCurrentFunction()
	return functionStack[#functionStack][1]
end
local fnTypes = {}
function ruleEnv.Register(origName, fn, ...)
	local name = string.lower(origName)
	if (executionEnv[name] ~= nil) then
		error("True for this item:'" .. name .. "' is already registered!")
	end
	local wrappedFn = wrapFunction(name, fn)
	for _, fnType in pairs({...}) do
		local map = fnTypes[fnType]
		if(map == nil) then
			map = {}
			fnTypes[fnType] = map
		end
		map[#map+1] = origName
	end
	executionEnv[name] = wrappedFn
	executionEnv[origName] = wrappedFn
	return fn
end

function ruleEnv.GetTagged(tag)
	return fnTypes[tag]
end

function ruleEnv.RegisterInternalRule(name, content)
	if(RbI.rules.internal[name] ~= nil) then
		error("Internal rule '"..name.."' is already registered!")
	end
	RbI.rules.internal[name] = RbI.BaseRule(name, false, content)
end


function ruleEnv.CreateUserRule(name)
	if(RbI.rules.internal[name] ~= nil) then
		error("User rule '"..name.."' is already registered!")
	end
	RbI.PersistentRule(name)
end

function ruleEnv.RenameUserRule(from, to)
	-- runtime rule
	RbI.rename(RbI.rules.user, from, to)
	RbI.rules.user[to].Rename(to) -- rulename and also RbI.rules.list
    -- rule persistence
	RbI.rename(RbI.account.rules, from, to)
end

function ruleEnv.RunRule(rule)
	local status, result = pcall(ruleEnv.EvaluateRule, rule)
	if(status == false) then
		RbI.RuleErrorHandler(rule, result)
	end
	return result
end

-- should be the only way to set/unset ruleEnv-bag/slot/itemLink/taskName-Variables
-- currently needs a safe call 'fn'-function which returns: status, result
-- currently no general error handling here :(
function ruleEnv.RunRuleWithItem(rule, bag, slot, itemLink, taskName)
	ctx.bag = bag
	ctx.slot = slot
	ctx.itemLink = itemLink
	ctx.taskName = taskName
	local status, result = pcall(ruleEnv.EvaluateRule, rule)
	ctx.bag = nil
	ctx.slot = nil
	ctx.itemLink = nil
	ctx.taskName = nil
	if(status == false) then
		RbI.RuleErrorHandler(rule, result)
	end
	return status, result
end

function ruleEnv.RunFunctionWithItem(bag, slot, itemLink, functionName, ...)
	ctx.bag = bag
	ctx.slot = slot
	ctx.itemLink = itemLink
	local status, result = pcall(executionEnv[functionName], ...)
	ctx.bag = nil
	ctx.slot = nil
	ctx.itemLink = nil
	if(status == false) then
		error(result)
	end
	return result
end

-- Looks at the rule runtime environment, gathers as much information as it can and returns it as string
local function getRunEnvironmentAsString()
	local constants = {}
	local functions = {}
	for key, value in pairs(executionEnv) do
		if(type(value) == 'function') then
			functions[key] = value
		elseif(not ignoreExecKeys[key]) then
			constants[key] = value
		end
	end
	local result = {}
	result[#result+1] = ">>Constants<<"
	for key, value in RbI.utils.spairs(constants) do
		result[#result+1] = ''..tostring(key)..' = '..RbI.utils.ArrayValuesToString(value)
	end

	result[#result+1] = "\n>>Functions<<"
	for key, value in RbI.utils.spairs(functions) do
		result[#result+1] = tostring(key)
	end
	return table.concat(result, '\n')
end

-- evaluate rule and fetch syntax errors
function ruleEnv.EvaluateRule(rule)
	local status, result = pcall(rule.Evaluate)
	if(not status) then
		if(type(result) == "string") then -- hide lua stacktrace, we have our own
			result = {
				message='Syntax error or unknown term.',
				stackTrace = ruleEnv.getStackTrace(),
				addition="Following constants/functions can be used:\n"..getRunEnvironmentAsString()..'\n\nFurther informations at: '..RbI.wikiLink
			}
		end
		if(result.ruleName == nil) then
			result.ruleName = rule.GetName()
		end
		if(result.ruleContent == nil) then
			result.ruleContent = rule.GetContent()
		end
		error(result)
	end
	return result
end

function ruleEnv.Test(ruleName, content)
	local status, result
	RbI.AddMessage('Rule ' .. ruleName, 'Start', ruleName, true)
	if(content == '') then
		content = '\n' -- force rule to be executed with an error, we want the syntax error output here
	end
	ctx.task = {}
	for _, bagFrom in pairs({BAG_BACKPACK, BAG_BANK, BAG_SUBSCRIBER_BANK, BAG_VIRTUAL, BAG_WORN, BAG_COMPANION_WORN}) do
		RbI.BagIteration(bagFrom, function(slotFrom)
			if(HasItemInSlot(bagFrom, slotFrom)) then
				local itemLink = GetItemLink(bagFrom, slotFrom)
				ctx.amountHelper = {}
				ctx.amountHelper.count = {}
				ctx.item = {
					bag = bagFrom,
					slot = slotFrom,
					link = itemLink
				}
				status, result = ruleEnv.RunRuleWithItem(RbI.BaseRule(ruleName, true, content) , bagFrom, slotFrom, itemLink)
				ctx.item = nil
				if(status) then
					if(result) then
						RbI.AddMessage('Rule ' .. ruleName, nil, 'RuleTest', true, nil, itemLink)
					end
				else
					return false -- break iteration
				end
			end
		end)
		if(status == false) then
			break
		end
	end
	ctx.task = nil
	local message
	if(status == nil) then
		message = 'Test found no items to test on'
	elseif(status == false) then
		message = 'Test canceled: An error ocurred!'
	else
		message = 'Test finished'
	end
	RbI.AddMessage('Rule ' .. ruleName, 'Final', nil, true, message)
	RbI.PrintOutput('Rule ' .. ruleName)
end

----------------------------------------------------------------------------
-- rule helper methods and errors ------------------------------------------
----------------------------------------------------------------------------

-- Default error with automatically attached stacktrace
function ruleEnv.error(msg, addition, notsupported)
	error({message=msg, stackTrace = ruleEnv.getStackTrace(), addition=addition, notsupported=notsupported})
end

function ruleEnv.errorAddonRequired(addonname)
	ruleEnv.error('Required addon \''..addonname..'\' was not found', nil, true)
end

function ruleEnv.errorNoInputsAllowed()
	ruleEnv.error("This function doesn't allow inputs")
end

-- deprecated use errorInvalidArgument2 instead
function ruleEnv.errorInvalidArgument(argIndex, input, validInputs, typeName)
	local addition;
	if (validInputs) then
		if (utils.isArray(validInputs)) then
			addition = 'Valid values are:\n' .. RbI.utils.ArrayValuesToString(validInputs)
		else
			addition = 'Valid values are:\n' .. RbI.utils.ArrayKeysToString(validInputs)
		end
	end
	typeName = typeName or 'Argument'
	if(argIndex ~= nil) then
		typeName = typeName..' #'..tostring(argIndex)
	end
	if(input == nil) then
		ruleEnv.error('No input was found', addition)
	else
		ruleEnv.error(typeName..': \'' .. tostring(input) .. '\' is not recognized.', addition)
	end
end

-- new errorInvalidArgument function that also look at language dependent keys
-- when given, 'dataTypeKey' has to be a valid RbI.dataType-key
local QUOTER = '\''
function ruleEnv.errorInvalidArgument2(argIndex, input, dataTypeKey, typeName)
	local addition;
	local validInputs = RbI.dataType[dataTypeKey]
	if (validInputs ~= nil) then
		if(utils.isArray(validInputs)) then -- like charnames
			addition = 'Valid values are:\n' .. RbI.dataKeyFixed(RbI.utils.ArrayValuesToString(validInputs))
		else
			local languageDependent = RbI.collectedData[dataTypeKey]
			if(languageDependent == nil) then
				addition = 'Valid values are:\n' .. RbI.dataKeyFixed(RbI.utils.ArrayKeysToString(validInputs))
			else
				local m = {}
				for key, value in pairs(validInputs) do
					if(dataTypeHandler.isStaticDataTypeKey(dataTypeKey, key)) then -- is not language dependent key
						local lKeys = dataTypeHandler.GetKeysForValues(languageDependent, value)
						if(#lKeys > 0) then
							m[#m+1] = RbI.dataKeyFixed(QUOTER..key..QUOTER)..', '..RbI.dataKeyLD(RbI.utils.ArrayValuesToString(lKeys, nil, QUOTER))..','
						else
							m[#m+1] = RbI.dataKeyFixed(QUOTER..key..QUOTER)..', '
						end
					end
				end
				addition = 'Valid values are:\n' .. table.concat(m, '\n')..'\n\n[Legend: '..RbI.dataKeyFixed("Static key")..' '..RbI.dataKeyLD('Language dependent')..']'
			end
		end
	end
	typeName = typeName or 'Argument'
	if(argIndex ~= nil) then
		typeName = typeName..' #'..tostring(argIndex)
	end
	if(input == nil) then
		ruleEnv.error('No input was found', addition)
	else
		ruleEnv.error(typeName..': \'' .. tostring(input) .. '\' is not recognized.', addition)
	end
end

-- some functions can only perform on specific items.
function ruleEnv.errorInvalidItem(needs)
	ruleEnv.error('Wrong item: '..ctx.itemLink..'\n'..GetCurrentFunction()..'() works only for \''..needs..'\'')
end

-- Ensures that the arguments have no nil value. (This could happen, when the user misspelled a variable or just forgot the apostrophe for a string)
-- With this method we accept varargs as long as predefined table-variables.
-- @param isRequired if true the function will throw an error, when there are no inputs in the varargs-argument
-- @param dataTypeKey optional, if set the data will be verified against the given dataType
-- @return argument[1], if argument[1] is a table
--         arguments as validated and mapped table, when dataTypeKey is given
--         arguments only lowered, otherwise
local function GetArguments(typeName, isRequired, dataTypeKey, addFn, ...)
	local validInputs = RbI.dataType[dataTypeKey]
	if(dataTypeKey ~= nil and validInputs == nil) then
		error("Validinputs required") -- fire as unexpected error
	end
	local args = {...}
	typeName = typeName or 'Argument'
	if(#args ~= select('#', ...)) then -- we got a nil value
		for i=1, select('#', ...) do
			if(select(i, ...) == nil) then
				ruleEnv.errorInvalidArgument2(i, nil, dataTypeKey, typeName)
			end
		end
	end
	if(#args == 0) then
		if(isRequired) then
			ruleEnv.errorInvalidArgument2(nil, nil, dataTypeKey, typeName)
		end
	else -- #args > 0
		if(type(args[1]) == "table") then
			return args[1] -- already validated and mapped
		end
		if(dataTypeKey == nil) then -- no mapping, only lower
			local result = {}
			for _, curArg in ipairs(args) do
				addFn(result, string.lower(curArg))
			end
			return result
		elseif(utils.isArray(validInputs)) then -- like char-names
			for i, curArg in pairs(args) do
				if(not utils.TableContainsValue(validInputs, curArg)) then
					ruleEnv.errorInvalidArgument2(i, curArg, dataTypeKey, typeName)
				end
			end
		else -- is data mapping
			local result = {}
			for i, curArg in ipairs(args) do
				curArg = string.lower(curArg)
				local mapped = validInputs[curArg]
				if(mapped == nil) then
					ruleEnv.errorInvalidArgument2(i, curArg, dataTypeKey, typeName)
				end
				addFn(result, mapped)
			end
			return result
		end
	end
	return args
end

local arrayAdd = function(table, entry)
	table[#table+1] = entry
end
function ruleEnv.GetArguments(typeName, isRequired, dataTypeKey, ...)
	return GetArguments(typeName, isRequired, dataTypeKey, arrayAdd, ...)
end
local mapAdd = function(table, entry)
	table[entry] = true
end
function ruleEnv.GetArgumentsMap(typeName, isRequired, dataTypeKey, ...)
	return GetArguments(typeName, isRequired, dataTypeKey, mapAdd, ...)
end

function ruleEnv.GetNumberArguments(typeName, isRequired, ...)
	local args = {...}
	if(#args ~= select('#', ...)) then -- we got a nil value
		for i=1, select('#', ...) do
			if(select(i, ...) == nil) then
				ruleEnv.errorInvalidArgument2(i, nil, nil, typeName)
			end
		end
	end
	if(#args == 0) then
		if(isRequired) then
			ruleEnv.errorInvalidArgument2(nil, nil, nil, typeName)
		end
	else -- #args > 0
		if(type(args[1]) == "table") then
			return args[1] -- already validated and mapped
		end
	end
	return args
end

-- @return the given ... characterNames as table (names lowered in rulestring)
--         if not given the defaults will be used
--         'defaultchars' for defaultAll = true
--         'me' for defaultAll = false
-- the names are validated if they are correctly written
local function GetCharactersNamesIntern(defaultAll, isOptional, ...)
	local charNames = ruleEnv.GetArguments('CharacterName', false, 'charName', ...)
	if(#charNames == 0) then
		if(defaultAll ~= nil) then
			if(defaultAll) then
				return executionEnv.defaultchars -- already validated
			else
				return {me} -- alreday validated
			end
		elseif(not isOptional) then
			ruleEnv.errorInvalidArgument2(nil, nil, 'charName', 'CharacterName')
		end
	elseif(defaultAll and #charNames == 1) then
		d("Hint: You are using an \'..inorder\'-function with only one argument. The function without 'inorder' will produce the same result, but is more efficient")
	end
	return charNames
end
function ruleEnv.GetCharactersNames(defaultAll, ...)
	return GetCharactersNamesIntern(defaultAll, false, ...)
end
function ruleEnv.GetCharactersNamesOptional(defaultAll, ...)
	return GetCharactersNamesIntern(defaultAll, true, ...)
end
