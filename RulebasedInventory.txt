﻿﻿## Title: RulebasedInventory
## APIVersion: 101032 101033
## Version: 2.24.9
## AddOnVersion: 224
## Author: TaxTalis, demawi
## Description: Bag, Bank, Junk, Deconstruct, Sell, Fence, Launder, Notify, Destroy. Rule-based!
## DependsOn: LibAddonMenu-2.0>=28
## OptionalDependsOn: FCOItemSaver LibPrice CraftStoreFixedAndImproved AutoCategory LibSets WritWorthy LibTextFilter LibCustomMenu
## SavedVariables: RbIAccount RbICharacter
##
## This Add-on is not created by, affiliated with or sponsored by ZeniMax Media Inc. or its affiliates. 
## The Elder Scrolls® and related logos are registered trademarks or trademarks of ZeniMax Media Inc. in the United States and/or other countries. 
## All rights reserved
##
## You can read the full terms at https://account.elderscrollsonline.com/add-on-terms
; See wiki for further addon informations and how to rule: https://gitlab.com/taxtalis/rulebased-inventory/-/wikis/Home

UI/Clipboard.xml
RulebasedInventory.lua
classes/Utilities.lua
classes/AccountSetting.lua
classes/CharacterSetting.lua
_classes/DataDumper.lua
Modules/Data.lua
Modules/DataCollected.lua
Modules/RuleEnvironment.lua
Modules/Functions_Item.lua
Modules/Functions_MultiCharacters.lua
Modules/Ext_CraftStore.lua
Modules/Rule.lua
Modules/RuleSet.lua
Modules/Lookup.lua
Modules/ActionQueue.lua
Modules/Action.lua
Modules/Output.lua
Modules/Task.lua
Modules/Profile.lua
Modules/Event.lua
Modules/Menu.lua
Modules/ItemInspector.lua