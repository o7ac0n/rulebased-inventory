-----------------------------------------------------------------------------------
-- Addon Name: Rulebased Inventory
-- Creator: TaxTalis, demawi
-- Addon Ideal: user-defined rule-based actions for inventory application
-- Addon Creation Date: September 11, 2018
--
-- File Name: Utilities.lua
-- File Description: This class offers helper functions.
-- Load Order Requirements: directly after RulebasedInventory
--
-----------------------------------------------------------------------------------

-- imports
local RbI = RulebasedInventory
local utils = {}
RbI.class.Utilities = utils -- static class
RbI.utils = utils -- shortcut

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- ESO-environment functions --------------------------------------------------
------------------------------------------------------------------------------

-- callback(matchedKey, value, originalFullKey)
function utils.readConstants(regex, callback)
    local constants = {}
    for key, value in zo_insecurePairs(_G) do
        if(not IsProtectedFunction(key) and not IsPrivateFunction(key)) then
            local match = key:match(regex)
            if(match ~= nil) then
                if(callback(match, value, key) == false) then
                    return
                end
            end
        end
    end
end

function utils.readConstantsAll(callback)
    utils.readConstants('(.*)', callback)
end

-- create a reader so we have to iterate only once over all constants
function utils.ConstanteReaderNew(baseLength)
    local callbacks = {}
    local onFinishCallbacks = {}

    local function addCallback(baseKey, matcher, callback, onFinish)
        if(baseKey:len() ~= baseLength) then
            error('Base has to be a length of '..baseLength..'!')
        end
        local callbackFns = callbacks[baseKey]
        if(callbackFns == nil) then
            callbackFns = {}
            callbacks[baseKey] = callbackFns
        end
        if(callbackFns[matcher] ~= nil) then
            error('Matcher already exists! '..baseKey..' '..matcher)
        end
        callbackFns[matcher] = callback
        if(onFinish) then
            table.insert(onFinishCallbacks, onFinish)
        end
    end

    local function read()
        utils.readConstantsAll(function(key, value)
            local baseKey = key:sub(1, baseLength)
            local callbackFns = callbacks[baseKey]
            if(callbackFns ~= nil) then
               for regex, callback in pairs(callbackFns) do
                   local match = key:match(regex)
                   if(match ~= nil) then
                       callback(match, value, key)
                   end
               end
            end
        end)
        for _, cb in ipairs(onFinishCallbacks) do cb() end
    end

    return {
        addCallback = addCallback,
        read = read,
    }
end



------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- General string functions ---------------------------------------------------
------------------------------------------------------------------------------

function utils.startsWith(String, Start)
    return string.sub(String,1,string.len(Start))==Start
end

function utils.endsWith(String, End)
    return string.sub(String,string.len(String)-string.len(End)+1)==End
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Table functions -----------------------------------------------------------
------------------------------------------------------------------------------

function utils.cloneFlat(obj, into)
    local result = into or {}
    for k, v in pairs(obj) do result[k] = v end
    return result
end

function utils.cloneDeep(obj, into)
    local result = into or {}
    for k, v in pairs(obj) do
        if(type(v) == 'table') then
            result[k] = utils.cloneDeep(v)
        else
            result[k] = v
        end
    end
    return result
end

-- appends t2 into t1
function utils.tableConcat(t1,t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end

function utils.isArray(t)
    return #t > 0
end

function utils.isArrayFull(t)
    local i = 0
    for _ in pairs(t) do
        i = i + 1
        if t[i] == nil then return false end
    end
    return true
end

function utils.count(t)
    local i = 0
    for _ in pairs(t) do
        i = i + 1
    end
    return i
end

-- function by M. Kottman, https://stackoverflow.com/questions/15706270/sort-a-table-in-lua [2018-07-29]
-- pairs in a sorted order
function utils.spairs(t, order)
    -- collect the keys
    local keys = {}
    for k in pairs(t) do keys[#keys+1] = k end

    -- if order function given, sort by it by passing the table and keys a, b,
    -- otherwise just sort the keys
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end

    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return keys[i], t[keys[i]]
        end
    end
end

-- @return concats the values of the map to a string
function utils.ArrayValuesToString(array, delimeter, stringQuoting)
    delimeter = delimeter or ', '
    stringQuoting = stringQuoting or '\''
    local result = {}
    for _, val in pairs(array) do
        if(type(val)=="string" and not withoutStringQuoting) then
            result[#result+1] = stringQuoting..val..stringQuoting
        else
            result[#result+1] = tostring(val)
        end
    end
    return table.concat(result, delimeter)
end

-- @return concats the keys of the map to a string
function utils.ArrayKeysToString(array, delimeter)
    delimeter = delimeter or ', '
    local result = {}
    for key in pairs(array) do
        result[#result+1] = '\''..tostring(key)..'\''
    end
    return table.concat(result, delimeter)
end

function utils.tableDiff(table1, table2)
    local result = {}
    if(#table1 ~= #table2) then
        result[#result+1] = 'Size: '..#table1..' <> '..#table2
    end
    if(utils.isArray(table1) ~= utils.isArray(table2)) then
        result[#result+1] = 'IsArray: '..tostring(utils.isArray(table1))..' <> '..tostring(utils.isArray(table2))
    end
    for key, value in pairs(table1) do
        if(table2[key] ~= value) then
            result[#result+1] = '\''..key..'\': '..tostring(value)..' <> '..tostring(table2[key])
        end
    end
    for key, value in pairs(table2) do
        if(table1[key] == nil) then
            result[#result+1] = '\''..key..'\': '..tostring(nil)..' <> '..tostring(value)
        end
    end
    table.insert(result, 1, 'Diffs found: '..#result)
    return result
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Table search functions ----------------------------------------------------
------------------------------------------------------------------------------

function utils.TableContainsLowercasedKey(lowerCasedKey, table)
    for key in pairs(table) do
        if(lowerCasedKey == key:lower()) then return true end
    end
    return false
end

function utils.TableGetWithLowercasedKey(table, lowerCasedKey)
    for key in pairs(table) do
        if(lowerCasedKey == key:lower()) then return key end
    end
end

function utils.TableContainsValue(table, element)
    for _, cur in ipairs(table) do
        if cur == element then return true end
    end
    return false
end

function utils.GetKeysForValue(value, data)
    local keys = {}
    for key, val in pairs(data) do
        if (val == value) then keys[#keys+1] = key end
    end
    return keys
end

function utils.GetKeyForValue(value, data)
    for key, val in pairs(data) do
        if (val == value) then return key end
    end
    return nil
end

function utils.TableToString(outTable)
    local out = {}
    for key, value in pairs(outTable) do
        out[#out+1] = key..": "..tostring(value)
    end
    return table.concat(out, "\n")
end

-- @return if one of the input matching one itemData in the list
function utils.IsIncludedInList(itemDatas, inputs)
    if(#inputs == 0) then
        if(#itemDatas == 0) then
            return true
        else
            return false
        end
    end
    for _, input in ipairs(inputs) do
        input = string.lower(input)
        for _, itemData in ipairs(itemDatas) do
            if(itemData == input) then return true end
        end
    end
    return false
end

function utils.TableFindIndex(curTable, element)
    for index, value in pairs(curTable) do
        if value == element then
            return index
        end
    end
end

function utils.TableRemoveElement(curTable, element)
    table.remove(curTable, utils.TableFindIndex(curTable, element) )
end

-- @param autoStart: when true queue starts automatically when an entry is pushed
function utils.NewAsyncQueue(autoStart, defaultDelay, nextFn, equalsFn, finishFn, startFn)
    local queue = {}
    local first = 0
    local last = -1
    local running = false
    local pauseFlag = false

    local startCallbacks = {}
    local finishCallbacks = {}

    local function OnStart()
        if (startFn) then startFn() end
        local temp = startCallbacks
        startCallbacks = {}
        for _, cb in pairs(temp) do cb() end
    end

    local function OnFinish()
        if(finishFn) then finishFn() end
        local temp = finishCallbacks
        finishCallbacks = {}
        for _, cb in pairs(temp) do cb() end
    end

    local function AddFinishCallback(callback)
        table.insert(finishCallbacks, callback)
    end

    local function AddStartCallback(callback)
        table.insert(startCallbacks, callback)
    end

    local function RemoveFinishCallback(callback)
        utils.TableRemoveElement(finishCallbacks, callback)
    end

    local function RemoveStartCallback(callback)
        utils.TableRemoveElement(startCallbacks, callback)
    end

    local function getDelay(entry)
        if(entry.time) then return math.max(1, entry.time - GetGameTimeMilliseconds()) end
        return defaultDelay
    end

    local Pop
    local function PopExecute()
        local entry = queue[first].entry
        queue[first] = nil
        first = first + 1
        nextFn(entry)
        if(pauseFlag) then
            running = false
        else
            Pop() -- keep running
        end
    end

    Pop = function(delay)
        if(first <= last) then
            zo_callLater(PopExecute, delay or getDelay(queue[first]))
        else -- empty
            running = false
            OnFinish()
        end
    end

    -- starts the queue, the first entry will only have 1 delay and not the defaultDelay
    local function Run()
        pauseFlag = false
        if(running == false) then
            running = true
            OnStart()
            Pop(1)
        end
    end

    local function Pause()
        pauseFlag = true
    end

    local function Push(entry, time)
        if(equalsFn) then
            for i = first, last do
                if(equalsFn(queue[i].entry, entry)) then
                    return
                end
            end
        end
        -- only if no entry was found
        last = last + 1
        queue[last] = {
            entry = entry
        }
        if(time) then
            queue[last].time = time + GetGameTimeMilliseconds()
        end
        if(autoStart) then Run() end
    end

    return {
        Run = Run,
        Push = Push,
        Pause = Pause,
        AddFinishCallback = AddFinishCallback,
        AddStartCallback = AddStartCallback,
        RemoveStartCallback = RemoveStartCallback,
        RemoveFinishCallback = RemoveFinishCallback,
        IsRunning = function() return running end,
    }

end