-----------------------------------------------------------------------------------
-- Addon Name: Rulebased Inventory
-- Creator: TaxTalis, demawi
-- Addon Ideal: user-defined rule-based actions for inventory application
-- Addon Creation Date: September 11, 2018
--
-- File Name: CharacterSetting.lua
-- File Description: This file contains the functions to edit character settings
-- Load Order Requirements: after RulebaseInventory
--
-----------------------------------------------------------------------------------

-- imports
local RbI = RulebasedInventory

-- statics
local characterSettings = {} -- loaded character settings

RbI.class.CharacterSetting = {

    -- constructor
    New = function(name, id, settingTable)
        local self = {}

        self.GetRaw = function()
            return settingTable
        end

        self.IsInitialised = function()
            return settingTable.profile ~= nil
        end

        function self.RenameRule(ruleName, ruleNameNew)
            if(self.IsInitialised()) then
                for taskName, ruleSetData in pairs(settingTable.profile) do
                    if(ruleSetData.ruleSet ~= nil) then
                        RbI.rename(ruleSetData.ruleSet, ruleName, ruleNameNew)
                    end
                end
            end
        end

        return self
    end,

    GetAll = function()
        return characterSettings
    end,

    -- static
    LoadAll = function()
        local thisCharSettings
        local function create(characterName, characterId, default)
            local default = {
                profileName = nil,
                profile = {}
            }
            return RbI.class.CharacterSetting.New(characterName, characterId, ZO_SavedVars:New("RbICharacter", RbI.savedVariableVersion, nil, default, nil, GetDisplayName(), characterName, characterId, ZO_SAVED_VARS_CHARACTER_ID_KEY))
        end
        for i = 1, GetNumCharacters() do
            local characterName, _, _, _, _, _, characterId = GetCharacterInfo(i)
            characterName = zo_strformat(SI_UNIT_NAME, characterName)
            local curSettings
            if(characterName == GetUnitName('player')) then
                curSettings = create(characterName, characterId, default)
                thisCharSettings = curSettings
            else
                curSettings = create(characterName, characterId)
            end
            characterSettings[characterName] = curSettings
        end
        return thisCharSettings, characterSettings
    end
}