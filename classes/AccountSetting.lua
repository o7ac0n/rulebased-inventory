-----------------------------------------------------------------------------------
-- Addon Name: Rulebased Inventory
-- Creator: TaxTalis, demawi
-- Addon Ideal: user-defined rule-based actions for inventory application
-- Addon Creation Date: September 11, 2018
--
-- File Name: AccountSetting.lua
-- File Description: This file contains the functions to edit account settings
-- Load Order Requirements: after RulebaseInventory
--
-----------------------------------------------------------------------------------

-- imports
local RbI = RulebasedInventory

-- statics
local accountSettings -- loaded account settings

RbI.class.AccountSetting = {

    -- constructor
    New = function(settingTable)
        local self = {}

        self.GetRaw = function()
            return settingTable
        end

        return self
    end,

    Get = function()
        return accountSettings
    end,

    -- static
    Load = function()
        -- default savedVariable
        local accountDefault = {
            settings = {
                safetyrule = true,
                fcois = (FCOIS ~= nil),
                taskStartOutput = true,
                taskActionOutput = true,
                taskSummaryOutput = true,
                taskFinishOutput = true,
                actionDelay = 1,
                taskDelay = 100,
                contextMenu = true,
                immediateExecutionAtCraftStation = true,
                casesensitiverules = false,
                startupitemcheck = false,
                outputcolor = "dbcd8a",
                -- usingRuleSets = _    -- will be set below
            },
            rules = {},
            profiles = {},
            ruleSets = {},
            tasks = {},
        }
        accountSettings = RbI.class.AccountSetting.New(ZO_SavedVars:NewAccountWide("RbIAccount", RbI.savedGlobalVariableVersion, nil, accountDefault))
        local settings = accountSettings.GetRaw().settings
        if (settings.usingRuleSets == nil) then
            settings.usingRuleSets = RbI.utils.count(accountSettings.GetRaw().ruleSets) > 0
        end

        return accountSettings
    end
}